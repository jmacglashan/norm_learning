package examples;

import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.learnfromdemo.mlirl.differentiableplanners.diffvinit.DifferentiableVInit;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunction;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class GridGameNormRFAgentRelate extends DifferentiableRF {

	protected RewardFunction teamFunction;
	protected Map<String, Integer> actionIndex;
	protected Policy teamPolicy;
	protected int numAgentActions;
	private boolean conflict;
	private boolean xRelate;
	private boolean yRelate;
	private boolean dist1;
	private boolean dist2;
	private boolean xGoalRelate0;
	private boolean yGoalRelate0;
	private boolean xGoalRelate1;
	private boolean yGoalRelate1;


	public GridGameNormRFAgentRelate(RewardFunction teamFunction, Policy teamPolicy, SGDomain domain, 
			boolean conflict, boolean xRelate, boolean yRelate, boolean dist1, boolean dist2, 
			boolean xGoalRelate0, boolean yGoalRelate0, boolean xGoalRelate1, boolean yGoalRelate1) {
		this.teamFunction = teamFunction;
		this.teamPolicy = teamPolicy;

		this.actionIndex = new HashMap<String, Integer>(domain.getAgentActions().size());
		for(SGAgentAction action : domain.getAgentActions()){
			this.actionIndex.put(action.actionName, actionIndex.size());
		}

		this.numAgentActions = this.actionIndex.size();
		this.conflict = conflict;
		this.xRelate = xRelate;
		this.yRelate = yRelate;
		this.dist1 = dist1;
		this.dist2 = dist2;
		this.xGoalRelate0 = xGoalRelate0;
		this.yGoalRelate0 = yGoalRelate0;
		this.xGoalRelate1 = xGoalRelate1;
		this.yGoalRelate1 = yGoalRelate1;
		int numVectorSections = getNumVectorSections();

		this.dim = 1 + numVectorSections*2*this.numAgentActions;
		this.parameters = new double[this.dim];
	}

	protected GridGameNormRFAgentRelate(RewardFunction teamFunction, Map<String, Integer> actionIndex, Policy teamPolicy, 
			boolean conflict, boolean xRelate, boolean yRelate, boolean dist1, boolean dist2) {

		this.conflict = conflict;
		this.xRelate = xRelate;
		this.yRelate = yRelate;
		this.dist1 = dist1;
		this.dist2 = dist2;
		int numVectorSections = getNumVectorSections();

		this.teamFunction = teamFunction;
		this.actionIndex = actionIndex;
		this.teamPolicy = teamPolicy;
		this.numAgentActions = this.actionIndex.size();

		this.dim = 1 + numVectorSections*2*this.numAgentActions;
	}

	private int getNumVectorSections() {
		int num = 0;
		if(this.conflict){
			num+=2;
		}
		if(this.xRelate){
			num+=3;
		}
		if(this.yRelate){
			num+=3;
		}
		if(this.dist1){
			num+=2;
		}
		if(this.dist2){
			num+=2;
		}
		if(this.xGoalRelate0){
			num+=3;
		}
		if(this.yGoalRelate0){
			num+=3;
		}
		if(this.xGoalRelate1){
			num+=3;
		}
		if(this.yGoalRelate1){
			num+=3;
		}
		return num;
	}

	@Override
	public double[] getGradient(State s, GroundedAction ga, State sp) {
		return this.stateActionFeatures(s, ga, sp);
	}

	@Override
	protected DifferentiableRF copyHelper() {
		return new GridGameNormRFAgentRelate(this.teamFunction, this.actionIndex, this.teamPolicy, 
				this.conflict, this.xRelate, this.yRelate, this.dist1, this.dist2);
	}

	@Override
	public double reward(State s, GroundedAction a, State sprime) {
		double [] features = this.stateActionFeatures(s, a, sprime);
		double val = this.dot(features, this.parameters);
		return val;
	}

	public double [] stateActionFeatures(State s, GroundedAction a, State sp){
		double [] fs = new double[this.dim];
		fs[0] = this.teamFunction.reward(s, a, sp);

		JointAction ja = ((CentralizedDomainGenerator.GroundedJointActionWrapper) a).jointAction;

		//no bonus features fire if it's not an optimal action
		if(this.teamPolicy.getProbOfAction(s, a) == 0.){
			return fs;
		}

		ObjectInstance [] obs = agentsAndGoals(s);

		//get player actions
		GroundedSGAgentAction p1Action = ja.action(obs[0].getName());
		GroundedSGAgentAction p2Action = ja.action(obs[1].getName());
		int indP1 = this.actionIndex.get(p1Action.actionName());
		int indP2 = this.actionIndex.get(p2Action.actionName()) + this.numAgentActions;


		double[]conflictFs = new double[4*this.numAgentActions];
		double[]xRelateFs = new double[6*this.numAgentActions];
		double[]yRelateFs = new double[6*this.numAgentActions];
		double[]dist1Fs = new double[4*this.numAgentActions];
		double[]dist2Fs = new double[4*this.numAgentActions];
		double[]xGoalRelate0Fs = new double[6*this.numAgentActions];
		double[]yGoalRelate0Fs = new double[6*this.numAgentActions];
		double[]xGoalRelate1Fs = new double[6*this.numAgentActions];
		double[]yGoalRelate1Fs = new double[6*this.numAgentActions];

		if(conflict){
			
			if(sameRow(obs) && conflict(obs)){
				//offset from team reward feature
				conflictFs[indP1] = 1.;
				conflictFs[indP2] = 1.;
			}
			else{
				//offset from team reward feature and the conflict-zone features
				conflictFs[2*this.numAgentActions + indP1] = 1.;
				conflictFs[2*this.numAgentActions + indP2] = 1.;
			}
		}
		if(xRelate){
			if(firstLeftOfSecond(obs[0], obs[1])){

				//offset from team reward feature
				xRelateFs[indP1] = 1.;
				xRelateFs[indP2] = 1.;

			}else if(firstLeftOfSecond(obs[1], obs[0])){
				//offset from team reward feature and the conflict-zone features
				xRelateFs[2*this.numAgentActions + indP1] = 1.;
				xRelateFs[2*this.numAgentActions + indP2] = 1.;
			}else{

				//offset from team reward feature and the conflict-zone features
				xRelateFs[3*this.numAgentActions + indP1] = 1.;
				xRelateFs[3*this.numAgentActions + indP2] = 1.;

			}
		}
		if(yRelate){
			if(firstBelowSecond(obs[0], obs[1])){

				//offset from team reward feature
				yRelateFs[indP1] = 1.;
				yRelateFs[indP2] = 1.;

			}else if(firstBelowSecond(obs[1], obs[0])){
				//offset from team reward feature and the conflict-zone features
				yRelateFs[2*this.numAgentActions + indP1] = 1.;
				yRelateFs[2*this.numAgentActions + indP2] = 1.;
			}else{

				//offset from team reward feature and the conflict-zone features
				yRelateFs[3*this.numAgentActions + indP1] = 1.;
				yRelateFs[3*this.numAgentActions + indP2] = 1.;

			}
		}
		if(dist1){
			if(manhattanDistOne(obs[0], obs[1])){

				//offset from team reward feature
				dist1Fs[indP1] = 1.;
				dist1Fs[indP2] = 1.;

			}else{
				//offset from team reward feature and the conflict-zone features
				dist1Fs[2*this.numAgentActions + indP1] = 1.;
				dist1Fs[2*this.numAgentActions + indP2] = 1.;
			}
		}
		if(dist2){
			if(manhattanDistTwo(obs[0], obs[1])){

				//offset from team reward feature
				dist2Fs[indP1] = 1.;
				dist2Fs[indP2] = 1.;

			}else{
				//offset from team reward feature and the conflict-zone features
				dist2Fs[2*this.numAgentActions + indP1] = 1.;
				dist2Fs[2*this.numAgentActions + indP2] = 1.;
			}
		}
		if(xGoalRelate0){
			if(firstLeftOfSecond(obs[0], obs[2])){
				xGoalRelate0Fs[indP1] = 1.;
				xGoalRelate0Fs[indP2] = 1.;
			}else if(firstLeftOfSecond(obs[2], obs[0])){
				xGoalRelate0Fs[2*this.numAgentActions + indP1] = 1.;
				xGoalRelate0Fs[2*this.numAgentActions + indP2] = 1.;
			}else{
				xGoalRelate0Fs[3*this.numAgentActions + indP1] = 1.;
				xGoalRelate0Fs[3*this.numAgentActions + indP2] = 1.;
			}
		}
		
		if(yGoalRelate0){
			if(firstBelowSecond(obs[0], obs[2])){
				yGoalRelate0Fs[indP1] = 1.;
				yGoalRelate0Fs[indP2] = 1.;
			}else if(firstBelowSecond(obs[2], obs[0])){
				yGoalRelate0Fs[2*this.numAgentActions + indP1] = 1.;
				yGoalRelate0Fs[2*this.numAgentActions + indP2] = 1.;
			}else{
				yGoalRelate0Fs[3*this.numAgentActions + indP1] = 1.;
				yGoalRelate0Fs[3*this.numAgentActions + indP2] = 1.;
			}
		}
		if(xGoalRelate1){
			if(firstLeftOfSecond(obs[1], obs[3])){
				xGoalRelate1Fs[indP1] = 1.;
				xGoalRelate1Fs[indP2] = 1.;
			}else if(firstLeftOfSecond(obs[3], obs[1])){
				xGoalRelate1Fs[2*this.numAgentActions + indP1] = 1.;
				xGoalRelate1Fs[2*this.numAgentActions + indP2] = 1.;
			}else{
				xGoalRelate1Fs[3*this.numAgentActions + indP1] = 1.;
				xGoalRelate1Fs[3*this.numAgentActions + indP2] = 1.;
			}
		}
		
		if(yGoalRelate1){
			if(firstBelowSecond(obs[1], obs[3])){
				yGoalRelate1Fs[indP1] = 1.;
				yGoalRelate1Fs[indP2] = 1.;
			}else if(firstBelowSecond(obs[3], obs[1])){
				yGoalRelate1Fs[2*this.numAgentActions + indP1] = 1.;
				yGoalRelate1Fs[2*this.numAgentActions + indP2] = 1.;
			}else{
				yGoalRelate1Fs[3*this.numAgentActions + indP1] = 1.;
				yGoalRelate1Fs[3*this.numAgentActions + indP2] = 1.;
			}
		}

		int index=1;

		if(conflict){
			index = addArray(conflictFs,index,fs);
		}
		if(xRelate){
			index = addArray(xRelateFs,index,fs);
		}
		if(yRelate){
			index = addArray(yRelateFs,index,fs);
		}
		if(dist1){
			index = addArray(dist1Fs,index,fs);
		}
		if(dist2){
			index = addArray(dist2Fs,index,fs);
		}
		if(xGoalRelate0){
			index = addArray(xGoalRelate0Fs, index, fs);
		}
		if(yGoalRelate0){
			index = addArray(yGoalRelate0Fs, index, fs);
		}
		if(xGoalRelate1){
			index = addArray(xGoalRelate1Fs, index, fs);
		}
		if(yGoalRelate1){
			index = addArray(yGoalRelate1Fs, index, fs);
		}

		return  fs;
	}

	private int addArray(double[] array, int index, double[] fs) {
		for(int i=0;i<array.length;i++){
			fs[index+i] = array[i];
		}
		return index+array.length;
	}

	public DifferentiableVInit createCorresponingDiffVInit(final ValueFunction sourceVF){

		return new DifferentiableVInit() {
			@Override
			public double[] getVGradient(State s) {
				double [] grad = new double[dim];
				grad[1] = sourceVF.value(s);
				return grad;
			}

			@Override
			public double[] getQGradient(State s, AbstractGroundedAction ga) {
				double [] grad = new double[dim];
				grad[1] = sourceVF.value(s);
				return grad;
			}

			@Override
			public double qValue(State s, AbstractGroundedAction a) {
				return sourceVF.value(s);
			}

			@Override
			public double value(State s) {
				return sourceVF.value(s);
			}
		};

	}

	public String verboseString(){
		StringBuilder buf = new StringBuilder();
		buf.append("Team: " + this.parameters[0] + "\n");

		Map<Integer,String> rev = this.reverseLookup();

		buf.append("conflict\n");
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent0:" + rev.get(i) + ": " + this.parameters[1 + i] + "\n");
		}
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent1:" + rev.get(i) + ": " + this.parameters[1 + this.numAgentActions + i] + "\n");
		}

		buf.append("bias\n");
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent0:" + rev.get(i) + ": " + this.parameters[1 + 2*this.numAgentActions +  i] + "\n");
		}
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent1:" + rev.get(i) + ": " + this.parameters[1 + 3*this.numAgentActions + i] + "\n");
		}

		return buf.toString();
	}

	protected boolean sameRow(ObjectInstance[] obs){

		int ay0 = obs[0].getIntValForAttribute(GridGame.ATTY);
		int ay1 = obs[1].getIntValForAttribute(GridGame.ATTY);

		return ay0 == ay1;
	}

	protected boolean conflict(ObjectInstance[] obs){


		int a0tg = this.dist(obs[0], obs[2]);
		int a0tog = this.dist(obs[0], obs[3]);

		int a1tg = this.dist(obs[1], obs[3]);
		int a1tog = this.dist(obs[1], obs[2]);

		return a0tog < a0tg && a1tog < a1tg;

	}

	protected Map<Integer, String> reverseLookup(){
		Map<Integer, String> rev = new HashMap<Integer, String>();
		for(Map.Entry<String, Integer> e : this.actionIndex.entrySet()){
			rev.put(e.getValue(), e.getKey());
		}
		return rev;
	}


	protected ObjectInstance[] agentsAndGoals(State s){
		List<ObjectInstance> aobs = s.getObjectsOfClass(GridGame.CLASSAGENT);
		ObjectInstance a0;
		ObjectInstance a1;
		if(aobs.get(0).getIntValForAttribute(GridGame.ATTPN) == 0){
			a0 = aobs.get(0);
			a1 = aobs.get(1);
		}
		else{
			a0 = aobs.get(1);
			a1 = aobs.get(0);
		}

		List<ObjectInstance> gobs = s.getObjectsOfClass(GridGame.CLASSGOAL);
		ObjectInstance g0;
		ObjectInstance g1;
		if(gobs.get(0).getIntValForAttribute(GridGame.ATTGT) == 1){
			g0 = gobs.get(0);
			g1 = gobs.get(1);
		}
		else{
			g0 = gobs.get(1);
			g1 = gobs.get(0);
		}

		return new ObjectInstance[]{a0, a1, g0, g1};
	}


	protected int dist(ObjectInstance a, ObjectInstance b){
		int ax = a.getIntValForAttribute(GridGame.ATTX);
		int ay = a.getIntValForAttribute(GridGame.ATTY);

		int bx = b.getIntValForAttribute(GridGame.ATTX);
		int by = b.getIntValForAttribute(GridGame.ATTY);

		int dist = Math.abs(ax-bx) + Math.abs(ay - by);

		return dist;
	}

	protected double dot(double [] v1, double [] v2){
		double sum = 0.;
		for(int i = 0; i < v1.length; i++){
			sum += v1[i] * v2[i];
		}
		return sum;
	}

	protected boolean firstCloserToGoal(ObjectInstance first, ObjectInstance firstGoal, 
			ObjectInstance second, ObjectInstance secondGoal){
		if(dist(first,firstGoal)<dist(second,secondGoal)){
			return true;
		} else {
			return false;
		}
	}

	protected boolean firstBelowSecond(ObjectInstance first, ObjectInstance second){
		if(first.getIntValForAttribute(GridGame.ATTY)<second.getIntValForAttribute(GridGame.ATTY)){
			return true;
		} else {
			return false;
		}
	}

	protected boolean firstLeftOfSecond(ObjectInstance first, ObjectInstance second){
		if(first.getIntValForAttribute(GridGame.ATTX)<second.getIntValForAttribute(GridGame.ATTX)){
			return true;
		} else {
			return false;
		}
	}

	protected boolean manhattanDistOne(ObjectInstance first, ObjectInstance second){
		if(dist(first,second)==1){
			return true;
		} else {
			return false;
		}
	}
	protected boolean manhattanDistTwo(ObjectInstance first, ObjectInstance second){
		if(dist(first,second)==2){
			return true;
		} else {
			return false;
		}
	}

}

