package examples;

import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.learnfromdemo.mlirl.differentiableplanners.diffvinit.DifferentiableVInit;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunction;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class GridGameNormRF2 extends DifferentiableRF {

	protected RewardFunction teamFunction;
	protected Map<String, Integer> actionIndex;
	protected Policy teamPolicy;
	protected int numAgentActions;


	public GridGameNormRF2(RewardFunction teamFunction, Policy teamPolicy, SGDomain domain) {
		this.teamFunction = teamFunction;
		this.teamPolicy = teamPolicy;

		this.actionIndex = new HashMap<String, Integer>(domain.getAgentActions().size());
		for(SGAgentAction action : domain.getAgentActions()){
			this.actionIndex.put(action.actionName, actionIndex.size());
		}

		this.numAgentActions = this.actionIndex.size();

		this.dim = 1 + 4*this.numAgentActions;
		this.parameters = new double[this.dim];
	}

	protected GridGameNormRF2(RewardFunction teamFunction, Map<String, Integer> actionIndex, Policy teamPolicy) {
		this.teamFunction = teamFunction;
		this.actionIndex = actionIndex;
		this.teamPolicy = teamPolicy;
		this.numAgentActions = this.actionIndex.size();

		this.dim = 1 + 4*this.numAgentActions;
	}

	@Override
	public double[] getGradient(State s, GroundedAction ga, State sp) {
		return this.stateActionFeatures(s, ga, sp);
	}

	@Override
	protected DifferentiableRF copyHelper() {
		return new GridGameNormRF2(this.teamFunction, this.actionIndex, this.teamPolicy);
	}

	@Override
	public double reward(State s, GroundedAction a, State sprime) {
		double [] features = this.stateActionFeatures(s, a, sprime);
		double val = this.dot(features, this.parameters);
		return val;
	}

	public double [] stateActionFeatures(State s, GroundedAction a, State sp){
		double [] fs = new double[this.dim];
		fs[0] = this.teamFunction.reward(s, a, sp);

		JointAction ja = ((CentralizedDomainGenerator.GroundedJointActionWrapper) a).jointAction;

		//no bonus features fire if it's not an optimal action
		if(this.teamPolicy.getProbOfAction(s, a) == 0.){
			return fs;
		}

		ObjectInstance [] obs = agentGoals(s);

		//get player actions
		GroundedSGAgentAction p1Action = ja.action(obs[0].getName());
		GroundedSGAgentAction p2Action = ja.action(obs[1].getName());
		int indP1 = this.actionIndex.get(p1Action.actionName());
		int indP2 = this.actionIndex.get(p2Action.actionName()) + this.numAgentActions;


		if(sameRow(obs) && conflict(obs)){

			//offset from team reward feature
			fs[1 + indP1] = 1.;
			fs[1 + indP2] = 1.;

		}
		else{

			//offset from team reward feature and the conflict-zone features
			fs[1 + 2*this.numAgentActions + indP1] = 1.;
			fs[1 + 2*this.numAgentActions + indP2] = 1.;

		}

		return  fs;
	}

	public DifferentiableVInit createCorresponingDiffVInit(final ValueFunction sourceVF){

		return new DifferentiableVInit() {
			@Override
			public double[] getVGradient(State s) {
				double [] grad = new double[dim];
				grad[1] = sourceVF.value(s);
				return grad;
			}

			@Override
			public double[] getQGradient(State s, AbstractGroundedAction ga) {
				double [] grad = new double[dim];
				grad[1] = sourceVF.value(s);
				return grad;
			}

			@Override
			public double qValue(State s, AbstractGroundedAction a) {
				return sourceVF.value(s);
			}

			@Override
			public double value(State s) {
				return sourceVF.value(s);
			}
		};

	}

	public String verboseString(){
		StringBuilder buf = new StringBuilder();
		buf.append("Team: " + this.parameters[0] + "\n");

		Map<Integer,String> rev = this.reverseLookup();

		buf.append("conflict\n");
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent0:" + rev.get(i) + ": " + this.parameters[1 + i] + "\n");
		}
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent1:" + rev.get(i) + ": " + this.parameters[1 + this.numAgentActions + i] + "\n");
		}

		buf.append("bias\n");
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent0:" + rev.get(i) + ": " + this.parameters[1 + 2*this.numAgentActions +  i] + "\n");
		}
		for(int i = 0; i < this.numAgentActions; i++){
			buf.append("    agent1:" + rev.get(i) + ": " + this.parameters[1 + 3*this.numAgentActions + i] + "\n");
		}

		return buf.toString();
	}

	protected boolean sameRow(ObjectInstance[] obs){

		int ay0 = obs[0].getIntValForAttribute(GridGame.ATTY);
		int ay1 = obs[1].getIntValForAttribute(GridGame.ATTY);

		return ay0 == ay1;
	}

	protected boolean conflict(ObjectInstance[] obs){


		int a0tg = this.dist(obs[0], obs[2]);
		int a0tog = this.dist(obs[0], obs[3]);

		int a1tg = this.dist(obs[1], obs[3]);
		int a1tog = this.dist(obs[1], obs[2]);

		return a0tog < a0tg && a1tog < a1tg;

	}

	protected Map<Integer, String> reverseLookup(){
		Map<Integer, String> rev = new HashMap<Integer, String>();
		for(Map.Entry<String, Integer> e : this.actionIndex.entrySet()){
			rev.put(e.getValue(), e.getKey());
		}
		return rev;
	}


	protected ObjectInstance [] agentGoals(State s){
		List<ObjectInstance> aobs = s.getObjectsOfClass(GridGame.CLASSAGENT);
		ObjectInstance a0;
		ObjectInstance a1;
		if(aobs.get(0).getIntValForAttribute(GridGame.ATTPN) == 0){
			a0 = aobs.get(0);
			a1 = aobs.get(1);
		}
		else{
			a0 = aobs.get(1);
			a1 = aobs.get(0);
		}

		List<ObjectInstance> gobs = s.getObjectsOfClass(GridGame.CLASSGOAL);
		ObjectInstance g0;
		ObjectInstance g1;
		if(gobs.get(0).getIntValForAttribute(GridGame.ATTGT) == 1){
			g0 = gobs.get(0);
			g1 = gobs.get(1);
		}
		else{
			g0 = gobs.get(1);
			g1 = gobs.get(0);
		}

		return new ObjectInstance[]{a0, a1, g0, g1};
	}


	protected int dist(ObjectInstance a, ObjectInstance b){
		int ax = a.getIntValForAttribute(GridGame.ATTX);
		int ay = a.getIntValForAttribute(GridGame.ATTY);

		int bx = b.getIntValForAttribute(GridGame.ATTX);
		int by = b.getIntValForAttribute(GridGame.ATTY);

		int dist = Math.abs(ax-bx) + Math.abs(ay - by);

		return dist;
	}

	protected double dot(double [] v1, double [] v2){
		double sum = 0.;
		for(int i = 0; i < v1.length; i++){
			sum += v1[i] * v2[i];
		}
		return sum;
	}
}
