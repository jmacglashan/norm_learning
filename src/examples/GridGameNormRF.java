package examples;

import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class GridGameNormRF extends DifferentiableRF {

	protected RewardFunction teamRF;
	Map<GroundedAction, Integer> actionIndex = new HashMap<GroundedAction, Integer>();
	List<GroundedAction> actionSet;
	TerminalFunction tf;
	Domain domain;

	public GridGameNormRF(RewardFunction teamRF, TerminalFunction tf, Domain domain, State seedState) {
		this.teamRF = teamRF;
		actionSet = Action.getAllApplicableGroundedActionsFromActionList(domain.getActions(), seedState);
		this.dim = actionSet.size()*2;
		this.parameters = new double[this.dim];
		for(GroundedAction ga : actionSet){
			this.getActionIndex(ga);
		}
		this.tf = tf;
		this.domain = domain;

	}

	@Override
	public double[] getGradient(State s, GroundedAction ga, State sp) {
		return this.stateActionFeatures(s, ga);
	}

	@Override
	protected DifferentiableRF copyHelper() {
		return null;
	}

	@Override
	public double reward(State s, GroundedAction a, State sprime) {

		double teamR = this.teamRF.reward(s, a, sprime);
		double [] saFeatures = this.stateActionFeatures(s, a);
		double bonus = this.dot(saFeatures, this.parameters);
		double fReward = teamR + bonus;

		return fReward;
	}

	public double [] stateActionFeatures(State s, GroundedAction a){


		double [] f = new double[this.dim];

		ObjectInstance [] obs = agentGoals(s);
		if(sameRow(obs)){

			if(conflict(obs)){
				f[getActionIndex(a)] = 1.;
			}
			else if(!this.gameEnder(s)){
				f[getActionIndex(a)+(this.dim/2)] = 1.;
			}

		}
		else if(!this.gameEnder(s)){
			f[getActionIndex(a)+(this.dim/2)] = 1.;
		}

		return f;
	}

	protected boolean gameEnder(State s){

		List<GroundedAction> allactions = Action.getAllApplicableGroundedActionsFromActionList(this.domain.getActions(), s);
		for(GroundedAction a : allactions){
			if(this.tf.isTerminal(a.executeIn(s))){
				return true;
			}
		}

		return false;

	}

	protected boolean sameRow(ObjectInstance [] obs){

		int ay0 = obs[0].getIntValForAttribute(GridGame.ATTY);
		int ay1 = obs[1].getIntValForAttribute(GridGame.ATTY);

		return ay0 == ay1;
	}

	protected boolean conflict(ObjectInstance[] obs){


		int a0tg = this.dist(obs[0], obs[2]);
		int a0tog = this.dist(obs[0], obs[3]);

		int a1tg = this.dist(obs[1], obs[3]);
		int a1tog = this.dist(obs[1], obs[2]);

		return a0tog < a0tg && a1tog < a1tg;

	}


	protected ObjectInstance [] agentGoals(State s){
		List<ObjectInstance> aobs = s.getObjectsOfClass(GridGame.CLASSAGENT);
		ObjectInstance a0;
		ObjectInstance a1;
		if(aobs.get(0).getIntValForAttribute(GridGame.ATTPN) == 0){
			a0 = aobs.get(0);
			a1 = aobs.get(1);
		}
		else{
			a0 = aobs.get(1);
			a1 = aobs.get(0);
		}

		List<ObjectInstance> gobs = s.getObjectsOfClass(GridGame.CLASSGOAL);
		ObjectInstance g0;
		ObjectInstance g1;
		if(gobs.get(0).getIntValForAttribute(GridGame.ATTGT) == 1){
			g0 = gobs.get(0);
			g1 = gobs.get(1);
		}
		else{
			g0 = gobs.get(1);
			g1 = gobs.get(0);
		}

		return new ObjectInstance[]{a0, a1, g0, g1};
	}


	protected int dist(ObjectInstance a, ObjectInstance b){
		int ax = a.getIntValForAttribute(GridGame.ATTX);
		int ay = a.getIntValForAttribute(GridGame.ATTY);

		int bx = b.getIntValForAttribute(GridGame.ATTX);
		int by = b.getIntValForAttribute(GridGame.ATTY);

		int dist = Math.abs(ax-bx) + Math.abs(ay - by);

		return dist;
	}

	protected double dot(double [] v1, double [] v2){
		double sum = 0.;
		for(int i = 0; i < v1.length; i++){
			sum += v1[i] * v2[i];
		}
		return sum;
	}

	public int getActionIndex(GroundedAction a){
		Integer ind = this.actionIndex.get(a);
		if(ind != null){
			return ind;
		}
		ind = this.actionIndex.size();
		this.actionIndex.put(a, ind);
		return ind;
	}


	public String verboseString(){
		StringBuilder buf = new StringBuilder();

		buf.append("Conflict\n");
		for(int i = 0; i < 25; i++){
			buf.append("  " + this.actionSet.get(i).toString() + ": " + this.parameters[i] + "\n");
		}
		buf.append("Bias\n");
		for(int i = 0; i < 25; i++){
			buf.append("  " + this.actionSet.get(i).toString() + ": " + this.parameters[i+25] + "\n");
		}

		return buf.toString();
	}

	public String stateActionFeaturesString(State s, GroundedAction a){

		double [] sf = this.stateActionFeatures(s, a);

		StringBuilder buf = new StringBuilder();

		buf.append("Conflict\n");
		for(int i = 0; i < 25; i++){
			buf.append("  " + this.actionSet.get(i).toString() + ": " + sf[i] + "\n");
		}
		buf.append("Bias\n");
		for(int i = 0; i < 25; i++){
			buf.append("  " + this.actionSet.get(i).toString() + ": " + sf[i+25] + "\n");
		}

		return buf.toString();

	}
}
