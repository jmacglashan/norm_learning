package examples;

import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.learnfromdemo.mlirl.differentiableplanners.diffvinit.DifferentiableVInit;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunction;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class GridGameNormRF2Cost extends GridGameNormRF2 {

	public GridGameNormRF2Cost(RewardFunction teamFunction, Policy teamPolicy, SGDomain domain) {
		super(teamFunction, teamPolicy, domain);
	}

	protected GridGameNormRF2Cost(RewardFunction teamFunction, Map<String, Integer> actionIndex, Policy teamPolicy) {
		super(teamFunction, actionIndex, teamPolicy);
	}

	// x = feature vector, theta = paramter vector
	// pf / pTheta = 2 * theta * x for first feature
	// pf / pTheta = -2 * theta * x for all but first feature
	@Override
	public double[] getGradient(State s, GroundedAction ga, State sp) {
		double[] features =  this.stateActionFeatures(s, ga, sp);
		double[] gradient = new double[features.length];
		for (int i = 0; i < features.length; i++) {
			double pDeriv = 2.0 * features[i] * this.parameters[i];
			gradient[i] = (i == 0) ? features[i] : -pDeriv;
		}
		System.out.println("Features:\t" + Arrays.toString(features));
		System.out.println("Parameters:\t" + Arrays.toString(this.parameters));
		System.out.println("Gradient:\t" + Arrays.toString(gradient));
		return gradient;
	}

	@Override
	protected DifferentiableRF copyHelper() {
		return new GridGameNormRF2Cost(this.teamFunction, this.actionIndex, this.teamPolicy);
	}

	// x = feature vector, theta = parameter vector
	// reward = -theta ^ 2 * x
	@Override
	public double reward(State s, GroundedAction a, State sprime) {
		double [] features = this.stateActionFeatures(s, a, sprime);
		if (features[0] > 0.0) {
			System.out.print("");
		}
		double [] parametersSquared = new double[this.parameters.length];
		for (int i = 0; i < this.parameters.length; i++) {
			double sq = this.parameters[i] * this.parameters[i];
			parametersSquared[i] = (i == 0) ? this.parameters[i] : -sq;
		}
		double val = this.dot(features, parametersSquared);
		System.out.println("Reward\t" + val);
		return val;
	}
}
