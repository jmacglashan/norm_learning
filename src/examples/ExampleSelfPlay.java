package examples;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.agents.HumanAgent;
import burlap.behavior.stochasticgames.agents.normlearning.NormLearningAgent;
import burlap.behavior.stochasticgames.auxiliary.GameSequenceVisualizer;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.debugtools.DPrint;
import burlap.domain.stochasticgames.gridgame.GGAltVis;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.domain.stochasticgames.gridgame.GridGameStandardMechanicsWithoutTieBreaking;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.World;
import burlap.oomdp.visualizer.Visualizer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class ExampleSelfPlay {

	SGDomain sgDomain;
	TerminalFunction tf;
	JointReward jr;
	List<SGAgentType> types;

	State s;

	World w;


	public ExampleSelfPlay() {

		//create grid game specification and world in which agents can play
		GridGame gg = new GridGame();
		this.sgDomain = (SGDomain)gg.generateDomain();
		this.sgDomain.setJointActionModel(new GridGameStandardMechanicsWithoutTieBreaking(sgDomain));
		this.s = this.getHallwayState();
		this.tf = new GridGame.GGTerminalFunction(sgDomain);
		this.jr = new GridGame.GGJointRewardFunction(sgDomain, 0, 10, true);

		this.w = new World(sgDomain, jr, tf, s);
		DPrint.toggleCode(this.w.getDebugId(), false);


		this.types = new ArrayList<SGAgentType>();
		types.add(GridGame.getStandardGridGameAgentType(sgDomain));



	}


	public void runNormAgents(){

		//convert grid game to a centralized MDP (which will be used to define social reward functions)
		CentralizedDomainGenerator mdpdg = new CentralizedDomainGenerator(sgDomain, types);
		Domain cmdp = mdpdg.generateDomain();
		RewardFunction crf = new TotalWelfare(jr);

		//create joint task planner for social reward function and RHIRL leaf node values
		final SparseSampling jplanner = new SparseSampling(cmdp, crf, tf, 0.99, new SimpleHashableStateFactory(), 20, -1);
		jplanner.toggleDebugPrinting(false);


		//create independent social reward functions to learn for each agent
		final GridGameNormRF2 agent1RF = new GridGameNormRF2(crf, new GreedyQPolicy(jplanner), this.sgDomain);
		final GridGameNormRF2 agent2RF = new GridGameNormRF2(crf, new GreedyQPolicy(jplanner), this.sgDomain);

		//create agents
		NormLearningAgent agent1 = new NormLearningAgent(this.sgDomain, agent1RF, -1, agent1RF.createCorresponingDiffVInit(jplanner), false);
		NormLearningAgent agent2 = new NormLearningAgent(this.sgDomain, agent2RF, -1, agent2RF.createCorresponingDiffVInit(jplanner), false);

		//have them join the world
		agent1.joinWorld(this.w, this.types.get(0));
		agent2.joinWorld(this.w, this.types.get(0));

		//play games
		List<GameAnalysis> games = new ArrayList<GameAnalysis>();
		for(int i = 0; i < 5; i++){
			System.out.println("running game " + i);
			GameAnalysis game = this.w.runGame(20);
			games.add(game);
		}

		//visualize results
		Visualizer v = GGAltVis.getVisualizer(5, 5);
		new GameSequenceVisualizer(v, this.sgDomain, games);


	}

	public void runHumanNormAgent(){

		Visualizer v = GGAltVis.getVisualizer(5, 5);
		HumanAgent human = new HumanAgent(v);
		human.addKeyAction("w", this.sgDomain.getSingleAction(GridGame.ACTIONNORTH).getAssociatedGroundedAction(""));
		human.addKeyAction("s", this.sgDomain.getSingleAction(GridGame.ACTIONSOUTH).getAssociatedGroundedAction(""));
		human.addKeyAction("d", this.sgDomain.getSingleAction(GridGame.ACTIONEAST).getAssociatedGroundedAction(""));
		human.addKeyAction("a", this.sgDomain.getSingleAction(GridGame.ACTIONWEST).getAssociatedGroundedAction(""));
		human.addKeyAction("x", this.sgDomain.getSingleAction(GridGame.ACTIONNOOP).getAssociatedGroundedAction(""));

		//convert grid game to a centralized MDP (which will be used to define social reward functions)
		CentralizedDomainGenerator mdpdg = new CentralizedDomainGenerator(sgDomain, types);
		Domain cmdp = mdpdg.generateDomain();
		RewardFunction crf = new TotalWelfare(jr);

		//create joint task planner for social reward function and RHIRL leaf node values
		final SparseSampling jplanner = new SparseSampling(cmdp, crf, tf, 0.99, new SimpleHashableStateFactory(), 20, -1);
		jplanner.toggleDebugPrinting(false);


		//create independent social reward functions to learn for each agent
		final GridGameNormRF2 agent1RF = new GridGameNormRF2(crf, new GreedyQPolicy(jplanner), this.sgDomain);

		NormLearningAgent agent1 = new NormLearningAgent(this.sgDomain, agent1RF, -1, 
				agent1RF.createCorresponingDiffVInit(jplanner), false);

		//have them join the world
		human.joinWorld(this.w, this.types.get(0));
		agent1.joinWorld(this.w, this.types.get(0));


		//play games
		List<GameAnalysis> games = new ArrayList<GameAnalysis>();
		for(int i = 0; i < 5; i++){
			System.out.println("running game " + i);
			GameAnalysis game = this.w.runGame(20);
			games.add(game);
		}


	}


	public State getHallwayState(){
		State s = GridGame.getCleanState(sgDomain, 2, 2, 2, 2, 5, 3);
		GridGame.setAgent(s, 0, 0, 1, 0);
		GridGame.setAgent(s, 1, 4, 1, 1);

		GridGame.setGoal(s, 0, 0, 1, 2);
		GridGame.setGoal(s, 1, 4, 1, 1);

		return s;
	}

	public static void main(String[] args) {

		ExampleSelfPlay ex = new ExampleSelfPlay();
		//ex.runNormAgents();
		ex.runHumanNormAgent();

	}

}
