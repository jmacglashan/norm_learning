package burlap.behavior.stochasticgames.auxiliary.jointmdp;

import burlap.behavior.policy.Policy;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator.GroundedJointActionWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ngopalan
 * This class converts a joint policy into a centralized policy in a centralized domain
 */
public class JointPolicyToCentralizedPolicy extends Policy{

	protected Policy jointPolicy;
	protected Domain cmdp;

	public JointPolicyToCentralizedPolicy(Policy jointPolicy, Domain cmdp) {
		this.jointPolicy = jointPolicy;
		this.cmdp = cmdp;
	}

	public Policy getJointPolicy() {
		return jointPolicy;
	}

	public void setJointPolicy(Policy jointPolicy) {
		this.jointPolicy = jointPolicy;
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		return new GroundedJointActionWrapper(this.cmdp.getActions().get(0), (JointAction)this.jointPolicy.getAction(s));
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		List<ActionProb> jointProbs = this.jointPolicy.getActionDistributionForState(s);
		List<ActionProb> saProbs = new ArrayList<ActionProb>(jointProbs.size());
		for(ActionProb ap : jointProbs){
			//System.out.println(ap.ga.actionName());
			saProbs.add(new ActionProb((new GroundedJointActionWrapper(this.cmdp.getActions().get(0), (JointAction)ap.ga)), ap.pSelection));
		}
		return saProbs;
	}

	@Override
	public boolean isStochastic() {
		return jointPolicy.isStochastic();
	}

	@Override
	public boolean isDefinedFor(State s) {
		return jointPolicy.isDefinedFor(s);
	}
}
