package burlap.behavior.stochasticgames.auxiliary.jointmdp;

import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.core.*;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.FullActionModel;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author James MacGlashan.
 */
public class CentralizedDomainGenerator implements DomainGenerator {

	public static final String WRAPPED_ACTION = "wrappedAction";

	SGDomain srcDomain;
	List<SGAgentType> agentTypes;


	public CentralizedDomainGenerator(SGDomain srcDomain, List<SGAgentType> agentTypes) {
		this.srcDomain = srcDomain;
		this.agentTypes = agentTypes;
	}

	public SGDomain getSrcDomain() {
		return srcDomain;
	}

	public void setSrcDomain(SGDomain srcDomain) {
		this.srcDomain = srcDomain;
	}

	public List<SGAgentType> getAgentTypes() {
		return agentTypes;
	}

	public void setAgentTypes(List<SGAgentType> agentTypes) {
		this.agentTypes = agentTypes;
	}

	@Override
	public Domain generateDomain() {

		SADomain domainWrapper = new SADomain();

		for(Attribute a : srcDomain.getAttributes()){
			domainWrapper.addAttribute(a);
		}

		for(ObjectClass c : srcDomain.getObjectClasses()){
			domainWrapper.addObjectClass(c);
		}

		for(PropositionalFunction pf : srcDomain.getPropFunctions()){
			domainWrapper.addPropositionalFunction(pf);
		}

		new JointActionWrapper(WRAPPED_ACTION, domainWrapper, this.srcDomain, this.agentTypes);

		return domainWrapper;

	}

	public static List<EpisodeAnalysis> gameAnalysesToEpisodeAnalyses(Domain cmdp, List<GameAnalysis> games){
		List<EpisodeAnalysis> episodes = new ArrayList<EpisodeAnalysis>(games.size());
		for(GameAnalysis ga : games){
			episodes.add(gameAnalysisToEpisodeAnalysis(cmdp, ga));
		}
		return episodes;
	}

	public static EpisodeAnalysis gameAnalysisToEpisodeAnalysis(Domain cmdp, GameAnalysis ga){

		EpisodeAnalysis ea = new EpisodeAnalysis(ga.getState(0));
		Action wrappedAction = cmdp.getActions().get(0);

		for(int t = 0; t < ga.maxTimeStep(); t++){
			GroundedJointActionWrapper gwrapped = new GroundedJointActionWrapper(wrappedAction, ga.getJointAction(t));
			State nstate = ga.getState(t+1);
			Map<String,Double> rs = ga.getJointReward(t+1);
			double sumReward = 0.;
			for(double d : rs.values()){
				sumReward += d;
			}

			ea.recordTransitionTo(gwrapped, nstate, sumReward);
		}

		return ea;
	}


	public static class JointActionWrapper extends Action implements FullActionModel{

		public Domain sgDomain;
		JointActionModel jam;
		List<SGAgentType> agentTypes;


		public JointActionWrapper(String name, Domain domain, SGDomain sgDomain, List<SGAgentType> agentTypes) {
			super(name, domain);
			this.jam = sgDomain.getJointActionModel();
			this.agentTypes = new ArrayList<SGAgentType>(agentTypes);
			this.sgDomain = sgDomain;
		}

		@Override
		public boolean applicableInState(State s, GroundedAction groundedAction) {

			JointAction ja = ((GroundedJointActionWrapper)groundedAction).jointAction;
			for(GroundedSGAgentAction gsa : ja){
				if(!gsa.applicableInState(s)){
					return false;
				}
			}

			return true;
		}

		@Override
		public boolean isPrimitive() {
			return true;
		}

		@Override
		public boolean isParameterized() {
			return true;
		}

		@Override
		public GroundedAction getAssociatedGroundedAction() {
			return new GroundedJointActionWrapper(this);
		}

		@Override
		public List<GroundedAction> getAllApplicableGroundedActions(State s) {

			List<JointAction> jas = JointAction.getAllJointActions(s, this.getAgents(s));
			List<GroundedAction> wjas = new ArrayList<GroundedAction>(jas.size());
			for(JointAction ja : jas){
				wjas.add(new GroundedJointActionWrapper(this, ja));
			}

			return wjas;
		}

		protected Map<String,SGAgentType> getAgents(State s){

			Map<String, SGAgentType> agents = new HashMap<String, SGAgentType>();
			for(SGAgentType at : this.agentTypes){
				List<ObjectInstance> agentObs = s.getObjectsOfClass(at.oclass.name);
				for(ObjectInstance agentOb : agentObs){
					agents.put(agentOb.getName(), at);
				}
			}

			return agents;

		}

		@Override
		protected State performActionHelper(State s, GroundedAction groundedAction) {
			JointAction ja = ((GroundedJointActionWrapper)groundedAction).jointAction;
			s = this.jam.performJointAction(s, ja);
			return s;
		}

		@Override
		public List<TransitionProbability> getTransitions(State s, GroundedAction groundedAction) {
			JointAction ja = ((GroundedJointActionWrapper)groundedAction).jointAction;
			List<TransitionProbability> transitions = this.jam.transitionProbsFor(s, ja);
			return transitions;
		}
	}


	public static class GroundedJointActionWrapper extends GroundedAction{

		public JointAction jointAction;

		public GroundedJointActionWrapper(Action action) {
			super(action);
			this.jointAction = new JointAction();
		}

		public GroundedJointActionWrapper(Action action, JointAction jointAction) {
			super(action);
			this.jointAction = jointAction;
		}

		@Override
		public GroundedAction copy() {
			return new GroundedJointActionWrapper(this.action, this.jointAction);
		}

		@Override
		public void initParamsWithStringRep(String[] params) {

			this.jointAction = new JointAction();
			for(String p : params){
				String [] gsaParams = p.split(":");
				GroundedSGAgentAction gsa = ((JointActionWrapper)this.action).sgDomain.getSingleAction(gsaParams[1]).getAssociatedGroundedAction(gsaParams[0]);
				this.jointAction.addAction(gsa);

			}
		}

		@Override
		public String[] getParametersAsString() {
			String [] params = new String[this.jointAction.size()];
			int i = 0;
			for(GroundedSGAgentAction sga : this.jointAction){
				params[i] = sga.toString();
				i++;
			}
			return params;
		}

		@Override
		public boolean equals(Object other) {
			if(this == other){
				return true;
			}

			if(!(other instanceof GroundedJointActionWrapper)){
				return false;
			}

			GroundedJointActionWrapper ow = (GroundedJointActionWrapper)other;
			return this.jointAction.equals(ow.jointAction);
		}

		@Override
		public int hashCode() {
			return this.toString().hashCode();
		}
	}


}
