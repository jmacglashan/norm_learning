package burlap.behavior.stochasticgames.auxiliary.jointmdp;

import burlap.behavior.policy.Policy;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;

import java.util.ArrayList;
import java.util.List;

/**
 * @author James MacGlashan.
 */
public class CentralizedPolicyToJointPolicy extends Policy{

	protected Policy centralizedPolicy;

	public CentralizedPolicyToJointPolicy(Policy centralizedPolicy) {
		this.centralizedPolicy = centralizedPolicy;
	}

	public Policy getCentralizedPolicy() {
		return centralizedPolicy;
	}

	public void setCentralizedPolicy(Policy centralizedPolicy) {
		this.centralizedPolicy = centralizedPolicy;
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		return ((CentralizedDomainGenerator.GroundedJointActionWrapper)centralizedPolicy.getAction(s)).jointAction;
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		List<ActionProb> srcProbs = this.centralizedPolicy.getActionDistributionForState(s);
		List<ActionProb> jProbs = new ArrayList<ActionProb>(srcProbs.size());
		for(ActionProb ap : srcProbs){
			jProbs.add(new ActionProb(((CentralizedDomainGenerator.GroundedJointActionWrapper)ap.ga).jointAction, ap.pSelection));
		}
		return jProbs;
	}

	@Override
	public boolean isStochastic() {
		return centralizedPolicy.isStochastic();
	}

	@Override
	public boolean isDefinedFor(State s) {
		return centralizedPolicy.isDefinedFor(s);
	}
}
