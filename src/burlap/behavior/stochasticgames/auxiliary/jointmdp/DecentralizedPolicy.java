package burlap.behavior.stochasticgames.auxiliary.jointmdp;

import burlap.behavior.policy.Policy;
import burlap.datastructures.HashedAggregator;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class DecentralizedPolicy extends Policy{

	protected Policy cpolicy;
	protected String agentName;


	public DecentralizedPolicy(Policy cpolicy) {
		this.cpolicy = cpolicy;
	}

	public DecentralizedPolicy(Policy cpolicy, String agentName) {
		this.cpolicy = cpolicy;
		this.agentName = agentName;
	}

	public Policy getCpolicy() {
		return cpolicy;
	}

	public void setCpolicy(Policy cpolicy) {
		this.cpolicy = cpolicy;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		JointAction ja = ((CentralizedDomainGenerator.GroundedJointActionWrapper)this.cpolicy.getAction(s)).jointAction;
		GroundedSGAgentAction myAction = ja.action(this.agentName);
		return myAction;
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {

		HashedAggregator<GroundedSGAgentAction> ag = new HashedAggregator<GroundedSGAgentAction>();
		List<ActionProb> jaProbs = this.cpolicy.getActionDistributionForState(s);
		for(ActionProb ap : jaProbs){
			JointAction ja = ((CentralizedDomainGenerator.GroundedJointActionWrapper)ap.ga).jointAction;
			GroundedSGAgentAction myAction = ja.action(this.agentName);
			ag.add(myAction, ap.pSelection);
		}

		List<ActionProb> finalps = new ArrayList<ActionProb>(ag.size());
		for(Map.Entry<GroundedSGAgentAction, Double> e : ag.entrySet()){
			finalps.add(new ActionProb(e.getKey(), e.getValue()));
		}

		return finalps;
	}
	
	public List<ActionProb> getJointActionDistributionForState(State s) {

		
		List<ActionProb> jaProbs = this.cpolicy.getActionDistributionForState(s);
		
		return jaProbs;
	}
	
	@Override
	public boolean isStochastic() {
		return cpolicy.isStochastic();
	}

	@Override
	public boolean isDefinedFor(State s) {
		return cpolicy.isDefinedFor(s);
	}
}
