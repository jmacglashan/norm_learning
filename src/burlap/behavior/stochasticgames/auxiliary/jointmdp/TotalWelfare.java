package burlap.behavior.stochasticgames.auxiliary.jointmdp;

import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.stochasticgames.JointReward;

import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class TotalWelfare implements RewardFunction {

	JointReward jr;

	public TotalWelfare(JointReward jr) {
		this.jr = jr;
	}

	@Override
	public double reward(State s, GroundedAction a, State sprime) {

		double sum = 0.;
		Map<String, Double> rs = this.jr.reward(s, ((CentralizedDomainGenerator.GroundedJointActionWrapper)a).jointAction, sprime);
		for(double d : rs.values()){
			sum += d;
		}
		return sum;
	}
}
