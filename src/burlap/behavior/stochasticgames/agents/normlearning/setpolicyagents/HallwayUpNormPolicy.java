package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import java.util.ArrayList;
import java.util.List;

import burlap.behavior.policy.Policy;
import burlap.behavior.stochasticgames.JointPolicy;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

public class HallwayUpNormPolicy extends JointPolicy {

	protected String targetAgentName;
	protected List<SGAgentAction> actions;
	protected double mistakeProbability, downEarlyProb;
	private Domain domain;

	public HallwayUpNormPolicy(Domain domain, double downEarlyProb) {
		this.domain = domain;
		setMistakeProbability(0.);
		setDownEarlyProb(downEarlyProb);
	}

	public HallwayUpNormPolicy(Domain domain, double downEarlyProb,
			String agentName) {
		this(domain, downEarlyProb);
		setTargetAgent(agentName);
	}

	public HallwayUpNormPolicy(Domain domain, double downEarlyProb,
			String agentName, double mistakeProbability) {
		this(domain, downEarlyProb, agentName);
		setMistakeProbability(mistakeProbability);
	}

	public HallwayUpNormPolicy(Domain domain, double downEarlyProb,
			double mistakeProbability) {
		this(domain, downEarlyProb);
		setMistakeProbability(mistakeProbability);
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		return sampleFromActionDistribution(s);
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		List<ActionProb> aps = new ArrayList<Policy.ActionProb>();
		ObjectInstance otherAgent = null, targetAgent;

		// Get target agent.
		targetAgent = s.getObject(targetAgentName);
		int agentX = targetAgent.getIntValForAttribute(GridWorldDomain.ATTX);
		int agentY = targetAgent.getIntValForAttribute(GridWorldDomain.ATTY);
		// Get target goal.
		int goalX = 0;
		for (ObjectInstance goalObject : s
				.getObjectsOfClass(GridGame.CLASSGOAL)) {
			if (goalObject.getIntValForAttribute(GridGame.ATTGT) - 1 == targetAgent
					.getIntValForAttribute(GridGame.ATTPN)) {
				goalX = goalObject.getIntValForAttribute(GridGame.ATTX);
			}
		}

		// Get opponent agent.
		List<ObjectInstance> agentsObjects = s
				.getObjectsOfClass(GridWorldDomain.CLASSAGENT);
		for (ObjectInstance others : agentsObjects) {
			if (others.getName() != targetAgentName)
				otherAgent = others;
		}
		int otherX = otherAgent.getIntValForAttribute(GridWorldDomain.ATTX);
		int otherY = otherAgent.getIntValForAttribute(GridWorldDomain.ATTY);

		int direction = (int) Math.signum(goalX - agentX);

		List<JointAction> allJAs = getAllJointActions(s);
		List<JointAction> potentialJAs = new ArrayList<JointAction>();

		// If both agents are in the same row, and the other agent is blocking
		// the targetAgent's goal, go N.
		if (otherY == agentY && direction * (otherX - agentX) > 0) {
			potentialJAs = getJointActionsForTargetAgent(GridGame.ACTIONNORTH,
					allJAs);
			for (JointAction ja : potentialJAs) {
				aps.add(new ActionProb(ja, 1.0 / potentialJAs.size()));
			}
		}
		// If the agents have passed and the top agent is in the top row,
		// move down to the center row with some probability.
		else if (direction * (otherX - agentX) < 0 && agentX == 2) {
			potentialJAs = getJointActionsForTargetAgent(GridGame.ACTIONSOUTH,
					allJAs);
			for (JointAction ja : potentialJAs) {
				aps.add(new ActionProb(ja, downEarlyProb / potentialJAs.size()));
			}
			potentialJAs = getJointActionsForTargetAgent(
					getActionForDirection(direction), allJAs);
			for (JointAction ja : potentialJAs) {
				aps.add(new ActionProb(ja, (1.0 - downEarlyProb)
						/ potentialJAs.size()));
			}
		}
		// If the agent is in the top row above goal, move South.
		else if (agentY == 2 && agentX == goalX) {
			potentialJAs = getJointActionsForTargetAgent(GridGame.ACTIONSOUTH,
					allJAs);
			for (JointAction ja : potentialJAs) {
				aps.add(new ActionProb(ja, 1.0 / potentialJAs.size()));
			}
		}
		// Otherwise move toward goal.
		else {
			potentialJAs = getJointActionsForTargetAgent(
					getActionForDirection(direction), allJAs);
			for (JointAction ja : potentialJAs) {
				aps.add(new ActionProb(ja, 1.0 / potentialJAs.size()));
			}
		}

		return aps;
	}

	private List<JointAction> getJointActionsForTargetAgent(
			String targetAction, List<JointAction> allJAs) {
		List<JointAction> potentialJAs = new ArrayList<JointAction>();
		for (JointAction ja : allJAs) {
			if (ja.action(targetAgentName).actionName() == targetAction)
				potentialJAs.add(ja);
		}
		return potentialJAs;
	}

	private String getActionForDirection(int direction) {
		String actionName;
		switch (direction) {
		case -1:
			actionName = GridGame.ACTIONWEST;
			break;
		case 1:
			actionName = GridGame.ACTIONEAST;
			break;
		default:
			actionName = null;
		}

		return actionName;
	}

	public double getMistakeProbability() {
		return mistakeProbability;
	}

	public void setMistakeProbability(double mistakeProbability) {
		this.mistakeProbability = mistakeProbability;
	}

	public double getDownEarlyProb() {
		return downEarlyProb;
	}

	public void setDownEarlyProb(double downEarlyProb) {
		this.downEarlyProb = downEarlyProb;
	}

	public void setActions(List<SGAgentAction> actions) {
		this.actions = actions;
	}

	@Override
	public void setTargetAgent(String agentName) {
		this.targetAgentName = agentName;
		setActions(domain.getAgentActions());
	}

	@Override
	public JointPolicy copy() {
		JointPolicy pol = new HallwayUpNormPolicy(domain, downEarlyProb,
				mistakeProbability);
		return pol;
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return SGAgentAction.getAllApplicableGroundedActionsFromActionList(s,
				targetAgentName, actions).size() > 0;
	}

}
