package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import burlap.behavior.stochasticgames.agents.SetStrategySGAgent;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

public class NormSetStrategyAgent extends SetStrategySGAgent {

	public NormSetStrategyAgent(SGDomain domain, NormJointPolicy policy) {
		super(domain, policy);
		
	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		JointAction ja = (JointAction)(this.policy.getAction(s));
		GroundedSGAgentAction actSelection = ja.action(this.getAgentName());
		return actSelection;
	}
	
	public NormJointPolicy getPolicy(){
		return (NormJointPolicy) this.policy;
	}
	
	

}
