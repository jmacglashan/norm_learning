package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import burlap.behavior.stochasticgames.JointPolicy;
import burlap.debugtools.RandomFactory;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.stochasticgames.agentactions.SGAgentAction;

public class RandomNormJointPolicy extends NormJointPolicy {
	
	/**
	 * The actions from which selection is performed
	 */
	protected List<SGAgentAction> actions;
	String targetAgent;

	/**
	 * The random factory used to randomly select actions.
	 */
	protected Random rand = RandomFactory.getMapped(0);


	/**
	 * Initializes by copying all the primitive actions references defined for the domain into an internal action
	 * list for this policy.
	 * @param domain the domain containing all the primitive actions.
	 */
	public RandomNormJointPolicy(SGDomain domain){
		this.actions = domain.getAgentActions();
	}

	/**
	 * Initializes by copying all the actions references defined in the provided list into an internal action
	 * list for this policy.
	 * @param acitons the actions to select between.
	 */
	public RandomNormJointPolicy(List<SGAgentAction> acitons){
		this.actions = new ArrayList<SGAgentAction>(actions);
	}


	/**
	 * Adds an action to consider in selection.
	 * @param action an action to consider in selection
	 */
	public void addAction(SGAgentAction action){
		this.actions.add(action);
	}


	/**
	 * Clears the action list used in action selection. Note that if no actions are added to this policy after
	 * calling this method then the policy will be undefined everywhere.
	 */
	public void clearActions(){
		this.actions.clear();
	}


	/**
	 * Removes an action from consideration.
	 * @param actionName the name of the action to remove.
	 */
	public void removeAction(String actionName){
		SGAgentAction toRemove = null;
		for(SGAgentAction a : this.actions){
			if(a.actionName.equals(actionName)){
				toRemove = a;
				break;
			}
		}
		if(toRemove != null){
			this.actions.remove(toRemove);
		}
	}

	/**
	 * Returns of the list of actions that can be randomly selected.
	 * @return the list of actions that can be randomly selected.
	 */
	public List<SGAgentAction> getSelectionActions(){
		return this.actions;
	}


	/**
	 * Returns the random generator used for action selection.
	 * @return the random generator used for action selection.
	 */
	public Random getRandomGenerator(){
		return this.rand;
	}


	/**
	 * Sets the random generator used for action selection.
	 * @param rand the random generator used for action selection.
	 */
	public void setRandomGenerator(Random rand){
		this.rand = rand;
	}


	@Override
	public AbstractGroundedAction getAction(State s) {
		List<GroundedSGAgentAction> gas = SGAgentAction.getAllApplicableGroundedActionsFromActionList(s,targetAgent,this.actions);
		if(gas.size() == 0){
			throw new PolicyUndefinedException();
		}
		GroundedSGAgentAction selection = gas.get(this.rand.nextInt(this.actions.size()));
		return selection;
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		List<GroundedSGAgentAction> gas = SGAgentAction.getAllApplicableGroundedActionsFromActionList(s,targetAgent,this.actions);
		if(gas.size() == 0){
			throw new PolicyUndefinedException();
		}
		double p = 1./gas.size();
		List<ActionProb> aps = new ArrayList<ActionProb>(gas.size());
		for(GroundedSGAgentAction ga : gas){
			ActionProb ap = new ActionProb(ga, p);
			aps.add(ap);
		}
		return aps;
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return SGAgentAction.getAllApplicableGroundedActionsFromActionList(s, targetAgent,this.actions).size() > 0;
	}

	@Override
	public void setTargetAgent(String agentName) {
		this.targetAgent = agentName;
		
	}

	@Override
	public JointPolicy copy() {

		return null;
	}

}
