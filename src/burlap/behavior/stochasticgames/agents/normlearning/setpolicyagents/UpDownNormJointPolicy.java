package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import burlap.behavior.stochasticgames.JointPolicy;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleGroundedSGAgentAction;

public class UpDownNormJointPolicy extends NormJointPolicy {

	protected Domain domain;
	Random rand;

	public UpDownNormJointPolicy(Domain domain, double mistakeProbability) {
		super();
		this.domain = domain;
		this.mistakeProbability = mistakeProbability;
		this.rand = new Random();
	}

	@Override
	public void setTargetAgent(String agentName) {
		// Does nothing.
	}

	@Override
	public JointPolicy copy() {
		return new UpDownNormJointPolicy(domain, mistakeProbability);
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		return sampleFromActionDistribution(s);
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {

		List<ActionProb> aps = new ArrayList<ActionProb>();
		Map<String, Integer> agentsX = new HashMap<String, Integer>();
		Map<String, Integer> agentsY = new HashMap<String, Integer>();
		Map<String, Integer> goalsX = new HashMap<String, Integer>();
		Map<String, Integer> goalsY = new HashMap<String, Integer>();

		for (ObjectInstance agentObject : s
				.getObjectsOfClass(GridWorldDomain.CLASSAGENT)) {
			agentsX.put(agentObject.getName(),
					agentObject.getIntValForAttribute(GridGame.ATTX));
			agentsY.put(agentObject.getName(),
					agentObject.getIntValForAttribute(GridGame.ATTY));
		}
		for (ObjectInstance goalObject : s
				.getObjectsOfClass(GridGame.CLASSGOAL)) {
			for (ObjectInstance agentObject : s
					.getObjectsOfClass(GridWorldDomain.CLASSAGENT)) {
				if (agentObject.getIntValForAttribute(GridGame.ATTPN) == goalObject
						.getIntValForAttribute(GridGame.ATTGT) - 1) {
					goalsX.put(agentObject.getName(),
							goalObject.getIntValForAttribute(GridGame.ATTX));
					goalsY.put(agentObject.getName(),
							goalObject.getIntValForAttribute(GridGame.ATTY));
				}
			}
		}

		JointAction ja = new JointAction();

		
		if(rand.nextDouble()<this.mistakeProbability){
			System.out.println("Setting random action 0");
			ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
					.getSGAgentAction(getActionNum(rand.nextInt(5)))));
		}
		if(rand.nextDouble()<this.mistakeProbability){
			System.out.println("Setting random action 1");
			ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
					.getSGAgentAction(getActionNum(rand.nextInt(5)))));
		}

		if(ja.action("agent0")==null){
			if (isNextToGoal(agentsX.get("agent0"), agentsY.get("agent0"),
					goalsX.get("agent0"), goalsY.get("agent0"))) {
				if (isNextToGoal(agentsX.get("agent1"), agentsY.get("agent1"),
						goalsX.get("agent1"), goalsY.get("agent1"))) {
					switch (agentsY.get("agent0")) {
					case 2:

						ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONSOUTH)));
						break;
					case 1:
						ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONEAST)));
						break;
					case 0:
						ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONNORTH)));
						break;
					default:

					}
				} else {
					ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
							.getSGAgentAction(GridGame.ACTIONNOOP)));
				}
			} else {
				switch (agentsY.get("agent0")) {
				case 2:
					ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
							.getSGAgentAction(GridGame.ACTIONEAST)));
					break;
				default:
					ja.addAction(new SimpleGroundedSGAgentAction("agent0", domain
							.getSGAgentAction(GridGame.ACTIONNORTH)));
				}
			}
		}
		if(ja.action("agent1")==null){
			if (isNextToGoal(agentsX.get("agent1"), agentsY.get("agent1"),
					goalsX.get("agent1"), goalsY.get("agent1"))) {
				if (isNextToGoal(agentsX.get("agent0"), agentsY.get("agent0"),
						goalsX.get("agent0"), goalsY.get("agent0"))) {
					switch (agentsY.get("agent1")) {
					case 2:

						ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONSOUTH)));
						break;
					case 1:
						ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONWEST)));
						break;
					case 0:
						ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONNORTH)));
						break;
					default:

					}
				} else {
					ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
							.getSGAgentAction(GridGame.ACTIONNOOP)));
				}
			} else {
				switch (agentsY.get("agent1")) {
				case 0:
					ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
							.getSGAgentAction(GridGame.ACTIONWEST)));
					break;
				default:
					ja.addAction(new SimpleGroundedSGAgentAction("agent1", domain
							.getSGAgentAction(GridGame.ACTIONSOUTH)));
				}
			}

		}
		aps.add(new ActionProb(ja, 1.0));

		return aps;
	}

	protected boolean isNextToGoal(int agentX, int agentY, int goalX, int goalY) {
		return (Math.abs(agentX - goalX) + Math.abs(agentY - goalY)) == 1;
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return true;
	}
	private String getActionNum(int actionNum) {
		switch(actionNum){
		case 0:
			return GridGame.ACTIONEAST;
		case 1:
			return GridGame.ACTIONNOOP;
		case 2:
			return GridGame.ACTIONNORTH;
		case 3:
			return GridGame.ACTIONSOUTH;
		case 4:
			return GridGame.ACTIONWEST;
		default:
			return GridGame.ACTIONNOOP;
		}
	}

}
