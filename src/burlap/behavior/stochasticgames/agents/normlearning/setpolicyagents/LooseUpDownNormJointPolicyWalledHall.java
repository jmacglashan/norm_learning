package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import burlap.behavior.stochasticgames.JointPolicy;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.agentactions.SimpleGroundedSGAgentAction;

public class LooseUpDownNormJointPolicyWalledHall extends NormJointPolicy {

	protected Domain domain;
	protected Random rand;

	public LooseUpDownNormJointPolicyWalledHall(Domain domain, double mistakeProbability) {
		super();
		this.domain = domain;
		this.mistakeProbability = mistakeProbability;
		this.rand = new Random();
	}

	@Override
	public void setTargetAgent(String agentName) {
		// Does nothing.
	}

	@Override
	public JointPolicy copy() {
		return new LooseUpDownNormJointPolicyWalledHall(domain, mistakeProbability);
	}

	@Override
	public AbstractGroundedAction getAction(State s) {
		return sampleFromActionDistribution(s);
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {

		List<ActionProb> aps = new ArrayList<ActionProb>();
		Map<String, Integer> agentsX = new HashMap<String, Integer>();
		Map<String, Integer> agentsY = new HashMap<String, Integer>();
		Map<String, Integer> goalsX = new HashMap<String, Integer>();
		Map<String, Integer> goalsY = new HashMap<String, Integer>();

		for (ObjectInstance agentObject : s
				.getObjectsOfClass(GridWorldDomain.CLASSAGENT)) {
			agentsX.put(agentObject.getName(),
					agentObject.getIntValForAttribute(GridGame.ATTX));
			agentsY.put(agentObject.getName(),
					agentObject.getIntValForAttribute(GridGame.ATTY));
		}
		for (ObjectInstance goalObject : s
				.getObjectsOfClass(GridGame.CLASSGOAL)) {
			for (ObjectInstance agentObject : s
					.getObjectsOfClass(GridWorldDomain.CLASSAGENT)) {
				if (agentObject.getIntValForAttribute(GridGame.ATTPN) == goalObject
						.getIntValForAttribute(GridGame.ATTGT) - 1) {
					goalsX.put(agentObject.getName(),
							goalObject.getIntValForAttribute(GridGame.ATTX));
					goalsY.put(agentObject.getName(),
							goalObject.getIntValForAttribute(GridGame.ATTY));
				}
			}
		}

		List<SimpleGroundedSGAgentAction> agent0Actions = new ArrayList<SimpleGroundedSGAgentAction>();
		List<SimpleGroundedSGAgentAction> agent1Actions = new ArrayList<SimpleGroundedSGAgentAction>();

		if(rand.nextDouble()<mistakeProbability){
			int action0 = rand.nextInt(5);
			agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
					.getSGAgentAction(getActionNum(action0))));

		}
		if(rand.nextDouble()<mistakeProbability){

			int action1 = rand.nextInt(5);
			agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
					.getSGAgentAction(getActionNum(action1))));
		}

		if(agent0Actions.isEmpty()){

			if (isNextToGoal(agentsX.get("agent0"), agentsY.get("agent0"),
					goalsX.get("agent0"), goalsY.get("agent0"))) {
				if (isNextToGoal(agentsX.get("agent1"), agentsY.get("agent1"),
						goalsX.get("agent1"), goalsY.get("agent1"))) {
					switch (agentsY.get("agent0")) {
					case 2:

						agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONSOUTH)));
						break;
					case 1:
						agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONEAST)));
						break;
					case 0:
						agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONNORTH)));
						break;
					default:

					}
				} else {
					agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
							.getSGAgentAction(GridGame.ACTIONNOOP)));
				}
			}else {
				if(agentsX.get("agent0")==goalsX.get("agent1")&& agentsY.get("agent0")==goalsY.get("agent1")){
					//at the start state
					agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
							.getSGAgentAction(GridGame.ACTIONEAST)));
				} else {
					switch (agentsY.get("agent0")) {
					case 2:
						if(agentsHavePassed(agentsX.get("agent0"),agentsX.get("agent1"),goalsX.get("agent0"))){
							agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
									.getSGAgentAction(GridGame.ACTIONSOUTH)));
						}
						if(agentsX.get("agent0")<3){
							agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
									.getSGAgentAction(GridGame.ACTIONEAST)));

							break;
						}
					default:
						agent0Actions.add(new SimpleGroundedSGAgentAction("agent0", domain
								.getSGAgentAction(GridGame.ACTIONNORTH)));
					}
				}
			}
		}

		if(agent1Actions.isEmpty()){

			if (isNextToGoal(agentsX.get("agent1"), agentsY.get("agent1"),
					goalsX.get("agent1"), goalsY.get("agent1"))) {
				if (isNextToGoal(agentsX.get("agent0"), agentsY.get("agent0"),
						goalsX.get("agent0"), goalsY.get("agent0"))) {
					switch (agentsY.get("agent1")) {
					case 2:

						agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONSOUTH)));
						break;
					case 1:
						agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONWEST)));
						break;
					case 0:
						agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONNORTH)));
						break;
					default:

					}
				} else {
					agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
							.getSGAgentAction(GridGame.ACTIONNOOP)));
				}
			} else {
				if(agentsX.get("agent1")==goalsX.get("agent0")&& agentsY.get("agent1")==goalsY.get("agent0")){
					//at the start state
					agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
							.getSGAgentAction(GridGame.ACTIONWEST)));
				} else {
					switch (agentsY.get("agent1")) {
					case 0:
						if(agentsHavePassed(agentsX.get("agent1"),agentsX.get("agent0"),goalsX.get("agent1"))){
							agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
									.getSGAgentAction(GridGame.ACTIONNORTH)));
						}
						if(agentsX.get("agent0")>1){
							agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
									.getSGAgentAction(GridGame.ACTIONWEST)));

							break;
						}
					default:
						agent1Actions.add(new SimpleGroundedSGAgentAction("agent1", domain
								.getSGAgentAction(GridGame.ACTIONSOUTH)));
					}
				}
			}

		}
		for(SimpleGroundedSGAgentAction a0 : agent0Actions){
			for(SimpleGroundedSGAgentAction a1 : agent1Actions){
				JointAction ja = new JointAction();
				ja.addAction(a0);
				ja.addAction(a1);
				aps.add(new ActionProb(ja, 1.0/(double)(agent1Actions.size()*agent0Actions.size())));
			}

		}
		return aps;
	}

	private String getActionNum(int actionNum) {
		switch(actionNum){
		case 0:
			return GridGame.ACTIONEAST;
		case 1:
			return GridGame.ACTIONNOOP;
		case 2:
			return GridGame.ACTIONNORTH;
		case 3:
			return GridGame.ACTIONSOUTH;
		case 4:
			return GridGame.ACTIONWEST;
		default:
			return GridGame.ACTIONNOOP;
		}
	}
	private boolean agentsHavePassed(int a0x, int a1x,
			int a0goalx) {
		if(Math.abs(a0x-a0goalx)<Math.abs(a1x-a0goalx)){
			return true;
		}
		return false;
	}

	protected boolean isNextToGoal(int agentX, int agentY, int goalX, int goalY) {
		return (Math.abs(agentX - goalX) + Math.abs(agentY - goalY)) == 1;
	}

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return true;
	}

}
