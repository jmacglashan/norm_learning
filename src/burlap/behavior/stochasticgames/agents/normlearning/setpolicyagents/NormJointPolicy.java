package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import java.util.List;

import burlap.behavior.stochasticgames.JointPolicy;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;

public abstract class NormJointPolicy extends JointPolicy {
	
	double mistakeProbability;

	@Override
	public abstract void setTargetAgent(String agentName);

	@Override
	public abstract JointPolicy copy();

	@Override
	public abstract AbstractGroundedAction getAction(State s);

	@Override
	public abstract List<ActionProb> getActionDistributionForState(State s);

	@Override
	public abstract boolean isStochastic();

	@Override
	public abstract boolean isDefinedFor(State s);
	
	public void setNoislessPolicy(){
		mistakeProbability = 0.0;
	}

	

}
