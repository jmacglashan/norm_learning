package burlap.behavior.stochasticgames.agents.normlearning.setpolicyagents;

import java.io.FileInputStream;
import java.util.Properties;

import burlap.behavior.policy.Policy;
import burlap.behavior.policy.RandomPolicy;
import burlap.oomdp.stochasticgames.SGDomain;

public class NormSetStrategyAgentFactory {

	public static NormSetStrategyAgent getSetStrategyAgent(String parametersFile, SGDomain domain) {

		Properties props = new Properties();
		FileInputStream fis;
		try {
			//loading properties from file
			fis = new FileInputStream(parametersFile+".properties");
			props.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String normType = props.getProperty("norm_type","up_down_norm");
		double mistakeProb = Double.parseDouble(props.getProperty("mistake_prob", "0.0"));

		NormJointPolicy policy = null;
		if(normType.compareTo("up_down_norm")==0){
			policy = new UpDownNormJointPolicy(domain, mistakeProb);
		}else if (normType.compareTo("loose_up_down_norm")==0){
			policy = new LooseUpDownNormJointPolicy(domain, mistakeProb);
		}else if (normType.compareTo("loose_wait_down_norm")==0){
			policy = new WaitDownNormJointPolicy(domain, mistakeProb);
		}else if(normType.compareTo("up_down_norm_h2")==0){
			//System.out.println("making policy");
			policy = new UpDownNormJointPolicyWalledHall(domain, mistakeProb);
		}else if (normType.compareTo("loose_up_down_norm_h2")==0){
			policy = new LooseUpDownNormJointPolicyWalledHall(domain, mistakeProb);
		}else if (normType.compareTo("loose_wait_down_norm_h2")==0){
			policy = new WaitDownNormJointPolicyWalledHall(domain, mistakeProb);
		}else if (normType.compareTo("random_policy")==0){
			policy = new RandomNormJointPolicy(domain);
		}
		return new NormSetStrategyAgent(domain, policy);

	}

}
