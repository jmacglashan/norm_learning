package burlap.behavior.stochasticgames.agents.normlearning;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.datastructures.AlphanumericSorting;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;

public class BatchTrainedNormLearningAgent extends NormLearningAgent {

	String batchTracesFolder;
	int numSamples = -1;
	String trial;
	List<EpisodeAnalysis> allGames;

	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public BatchTrainedNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int jointPlannerH,
			int c, String batchTracesFolder, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, jointPlannerH, c, learnFromBadGames);
		this.batchTracesFolder = batchTracesFolder;

	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 * @param numSamples 
	 */
	public BatchTrainedNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, 
			ValueFunctionInitialization leafValues, String batchTracesFolder, String trial, int numSamples, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, c, leafValues, learnFromBadGames);
		this.batchTracesFolder = batchTracesFolder;
		this.trial = trial;
		this.numSamples = numSamples;


	}

	@Override
	public void gameStarting() {
		//System.out.println("game starting");
		if(!this.started) {

			this.generateEquivelentSADomain();
			//this.allGames = createEpisodeAnalysisList();
			this.cgames = createEpisodeAnalysisList(); //allGames;
			if(cgames.size()==0){
				this.dp = this.generateNoExperiencePolicy();
			}else{
				this.dp = this.generateExperiencedPolicy(cgames);
			}
			this.started = true;
		}

		System.out.println("CGAMES SIZE: "+cgames.size()+"______________________________________________");

		this.gameStart = true;
	}


	@Override
	public void gameTerminated() {
		//System.out.println("Game terminated");
		System.out.println(this.currentGame);
		EpisodeAnalysis ea = CentralizedDomainGenerator.gameAnalysisToEpisodeAnalysis(this.cmdpDomain, this.currentGame);
		double reward = ea.getDiscountedReturn(1.0);
		if (reward > 0.0 || learnFromBadGames) {
			//this.cgames.add(ea);
		} 
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction, Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		//System.out.println("Observing outcome");
		if(this.gameStart){
			this.currentGame = new GameAnalysis(s);
			System.out.println(this.currentGame);
			this.gameStart = false;
		}

		this.currentGame.recordTransitionTo(jointAction, sprime, jointReward);
	}


	protected List<EpisodeAnalysis> createEpisodeAnalysisList(){
		//System.out.println("creating list");
		if(!batchTracesFolder.endsWith("/")){
			batchTracesFolder = batchTracesFolder + "/";
		}
		//System.out.println("Dir: "+batchTracesFolder);

		File dir = new File(batchTracesFolder);
		final String ext = ".game";

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if(name.endsWith(ext)&&name.contains("trial_"+trial)){
					return true;
				}
				return false;
			}
		};
		String[] children = dir.list(filter);
		//System.out.println("dir: "+dir.list(filter));
		List<EpisodeAnalysis> eas = new ArrayList<EpisodeAnalysis>(children.length);

		for(int i = 0; i < children.length; i++){
			String gameFile = batchTracesFolder + children[i];
			GameAnalysis ga = GameAnalysis.parseFileIntoGA(gameFile, sgDomain);
			EpisodeAnalysis ea = CentralizedDomainGenerator.
					gameAnalysisToEpisodeAnalysis(cmdpDomain, ga);
			eas.add(ea);
		}
		//System.out.println("Read in "+eas.size()+" games.");
		Random rand = new Random();
		if(numSamples>=0){
			while(eas.size()>this.numSamples){
				eas.remove(rand.nextInt(eas.size()));
			}
		}
		return eas;
	}

}
