package burlap.behavior.stochasticgames.agents.normlearning;

import java.io.FileInputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import examples.GridGameNormRF2;
import examples.GridGameNormRFAgentRelate;


public class NormLearningAgentFactory {

	public static NormLearningAgent getNormLearningAgent(String parametersFile, String outputFile, String trial, int numSamples, SGDomain sgDomain, 
			List<SGAgentType> types, JointReward jr, TerminalFunction tf){

		Properties props = new Properties();
		FileInputStream fis;
		try {
			//loading properties from file
			fis = new FileInputStream(parametersFile+".properties");
			props.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// convert grid game to a centralized MDP (which will be used to define
		// social reward functions)
		CentralizedDomainGenerator mdpdg = new CentralizedDomainGenerator(sgDomain, types);
		Domain cmdp = mdpdg.generateDomain();
		RewardFunction crf = new TotalWelfare(jr);

		// read from parameters file
		double gamma = Double.parseDouble(props.getProperty("gamma","0.99"));
		int h = Integer.parseInt(props.getProperty("h","20"));
		int c = Integer.parseInt(props.getProperty("c","-1"));

		// create joint task planner for social reward function and RHIRL leaf
		// node values
		final SparseSampling jplanner = new SparseSampling(cmdp, crf, tf, gamma,
				new SimpleHashableStateFactory(), h, c);
		jplanner.toggleDebugPrinting(false);


		//final GridGameNormRF2 RF= new GridGameNormRF2(crf,
			//	new GreedyQPolicy(jplanner), sgDomain);

		//final GridGameNormRFAgentRelate RF = new GridGameNormRFAgentRelate(crf, new GreedyQPolicy(jplanner), sgDomain, 
			//false, true, true, false, false, false, false, false, false);
		
	


		// create independent social reward functions to learn for each agent


		// read in more parameters from properties file
		boolean learnFromZeroScoreGames = Boolean.parseBoolean(props.getProperty("learn_from_zero_games", "true"));
		boolean useTeamPlanToExplore = Boolean.parseBoolean(props.getProperty("use_team_plan_explore", "true"));
		boolean explore = Boolean.parseBoolean(props.getProperty("explore", "false"));
		boolean batch = Boolean.parseBoolean(props.getProperty("batch", "false"));
		String batchFolder = props.getProperty("batch_folder", "none");
		
		boolean two_batches = Boolean.parseBoolean(props.getProperty("two_batches", "false"));
		String batchFolder1 = props.getProperty("batch_folder_1", "none");
		String batchFolder2 = props.getProperty("batch_folder_2", "none");

		//System.out.println("SETTING PARAMS:"+batchFolder);
		if(batchFolder.compareTo("none")==0 && batch){
			batch = false;
			System.err.print(" No batch location specified, so continuing without batch learning");
		} else if(batch && batchFolder.compareTo("experiment_directory")==0){
			batchFolder = outputFile;
		}else if(two_batches && batchFolder.compareTo("experiment_directory")==0){
			batchFolder1 = outputFile;
			batchFolder2 = outputFile;
			//System.out.println("SETTING OUTPUT FILE");
		}
		boolean conflict = Boolean.parseBoolean(props.getProperty("conflict", "true"));
		boolean xRelate = Boolean.parseBoolean(props.getProperty("xRelate", "true"));
		boolean yRelate= Boolean.parseBoolean(props.getProperty("yRelate", "true"));
		boolean dist1 = Boolean.parseBoolean(props.getProperty("dist1", "true"));
		boolean dist2 = Boolean.parseBoolean(props.getProperty("dist2", "true"));
		boolean xGoalRelate0 = Boolean.parseBoolean(props.getProperty("xGoalRelate0", "true"));
		boolean yGoalRelate0 = Boolean.parseBoolean(props.getProperty("yGoalRelate0", "true"));
		boolean xGoalRelate1 = Boolean.parseBoolean(props.getProperty("xGoalRelate1", "true"));
		boolean yGoalRelate1 = Boolean.parseBoolean(props.getProperty("yGoalRelate1", "true"));
		//int numSamples_param = Integer.parseInt(props.getProperty("num_samples", "-1"));
		
		
		//System.out.println("Conflict: "+conflict+"________________________________________");
		//System.out.println("yGoalRelate1: "+yGoalRelate1+"________________________________________");
		
		final GridGameNormRFAgentRelate RF = new GridGameNormRFAgentRelate(crf, new GreedyQPolicy(jplanner), sgDomain, 
				conflict, xRelate, yRelate, dist1, dist2, xGoalRelate0, yGoalRelate0, xGoalRelate1, yGoalRelate1);

		// create agent
		NormLearningAgent agent;
		if(batch) {
			agent = new BatchTrainedNormLearningAgent(sgDomain, RF, c,
					RF.createCorresponingDiffVInit(jplanner), batchFolder, trial, numSamples, learnFromZeroScoreGames, useTeamPlanToExplore);
		} else if(two_batches){
			//System.out.println("numSamples: "+numSamples);
			agent = new TwoBatchTrainedNormLearningAgent(sgDomain, RF, c,
					RF.createCorresponingDiffVInit(jplanner), batchFolder1, batchFolder2,trial, -1, numSamples, learnFromZeroScoreGames, useTeamPlanToExplore);
		} else if(explore) {
			agent = new ExploringNormLearningAgent(sgDomain, RF, c,
					RF.createCorresponingDiffVInit(jplanner), learnFromZeroScoreGames, useTeamPlanToExplore);
		} else {
			agent = new NormLearningAgent(sgDomain, RF, c,
					RF.createCorresponingDiffVInit(jplanner), learnFromZeroScoreGames);
		}
		return agent;
	}

}
