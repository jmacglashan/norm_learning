package burlap.behavior.stochasticgames.agents.normlearning.modelbasedagents;

import java.io.FileInputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import examples.GridGameNormRF2;
import examples.GridGameNormRFAgentRelate;


public class ModelBasedAgentFactory {

	public static ModelBasedLearningAgent getNormLearningAgent(String parametersFile, String outputFile, SGDomain sgDomain, 
			List<SGAgentType> types, JointReward jr, TerminalFunction tf){

		Properties props = new Properties();
		FileInputStream fis;
		try {
			//loading properties from file
			fis = new FileInputStream(parametersFile+".properties");
			props.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// convert grid game to a centralized MDP (which will be used to define
		// social reward functions)
		CentralizedDomainGenerator mdpdg = new CentralizedDomainGenerator(sgDomain, types);
		Domain cmdp = mdpdg.generateDomain();
		RewardFunction crf = new TotalWelfare(jr);

		// read from parameters file
		double gamma = Double.parseDouble(props.getProperty("gamma","0.99"));
		int h = Integer.parseInt(props.getProperty("h","20"));
		int c = Integer.parseInt(props.getProperty("c","-1"));

		// create joint task planner for social reward function and RHIRL leaf
		// node values
		final SparseSampling jplanner = new SparseSampling(cmdp, crf, tf, gamma,
				new SimpleHashableStateFactory(), h, c);
		jplanner.toggleDebugPrinting(false);


		//final GridGameNormRF2 RF= new GridGameNormRF2(crf,
			//	new GreedyQPolicy(jplanner), sgDomain);

		final GridGameNormRFAgentRelate RF = new GridGameNormRFAgentRelate(crf, new GreedyQPolicy(jplanner), sgDomain, 
			false, true, true, false, false, true, true, true, true);


		// create independent social reward functions to learn for each agent


		boolean batch = Boolean.parseBoolean(props.getProperty("batch", "false"));
		String batchFolder = props.getProperty("batch_folder", "none");

		if(batchFolder.compareTo("none")==0 && batch){
			batch = false;
			System.err.print(" No batch location specified, so continuing without batch learning");
		} else if(batchFolder.compareTo("experiment_directory")==0){
			batchFolder = outputFile;
		}

		// create agent
		ModelBasedLearningAgent agent = new ModelBasedLearningAgent();
		
		return agent;
	}

}
