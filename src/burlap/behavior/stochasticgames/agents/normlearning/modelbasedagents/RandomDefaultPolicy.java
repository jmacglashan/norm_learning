package burlap.behavior.stochasticgames.agents.normlearning.modelbasedagents;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;


import burlap.behavior.policy.Policy;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

public class RandomDefaultPolicy extends Policy {

	Map<State, List<ActionProb>> setActionDistributions;
	List<AbstractGroundedAction> availableActions;

	Random rand = new Random();

	@Override
	public AbstractGroundedAction getAction(State s) {
		if(setActionDistributions.containsKey(s)){
			return pickActionFromDistribution(setActionDistributions.get(s));
		} else {
			return availableActions.get(rand.nextInt(availableActions.size()));
		}

	}

	private AbstractGroundedAction pickActionFromDistribution(
			List<ActionProb> actionDistribution) {
		double draw = rand.nextDouble();
		double valSoFar = 0.0;
		for(ActionProb prob : actionDistribution){
			valSoFar+=prob.pSelection;
			if(valSoFar>=draw){
				return prob.ga;
			}
		}
		return null;
	}

	@Override
	public List<ActionProb> getActionDistributionForState(State s) {
		if (setActionDistributions.containsKey(s)){
			return setActionDistributions.get(s);
		} else {
			List<ActionProb> newActionProbs = new ArrayList<ActionProb>();
			for (AbstractGroundedAction action : availableActions){
				ActionProb ap = new ActionProb(action, 1.0/(double)availableActions.size());
				newActionProbs.add(ap);
			}
			return newActionProbs;
		}
	}
	
	public void updatePolicyWithCounts(Map<State, Map<GroundedSGAgentAction,Double>> actionCounts){
		//Map<State, List<ActionProb>>
		for(State s : actionCounts.keySet()){
			double total = 0.0;
			for(Map.Entry<GroundedSGAgentAction, Double> e : actionCounts.get(s).entrySet()){
				total+=e.getValue();
			}
			List<ActionProb> aps = new ArrayList<ActionProb>();
			for(Map.Entry<GroundedSGAgentAction, Double> e : actionCounts.get(s).entrySet()){
				ActionProb ap = new ActionProb(e.getKey(),e.getValue()/total);
				aps.add(ap);
			}
			setActionDistributions.put(s, aps);
		}
	}
	

	@Override
	public boolean isStochastic() {
		return true;
	}

	@Override
	public boolean isDefinedFor(State s) {
		return true;
	}

}
