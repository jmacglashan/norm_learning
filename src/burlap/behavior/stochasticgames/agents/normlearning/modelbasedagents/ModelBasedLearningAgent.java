package burlap.behavior.stochasticgames.agents.normlearning.modelbasedagents;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import burlap.behavior.policy.Policy;
import burlap.domain.stochasticgames.gridgame.GridGame;
import burlap.oomdp.core.objects.ObjectInstance;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

/**
 * This is an agent that is based on Mark Ho's model-based, simple cognitive-hierarchy reinforcement-learning agent.
 * 
 * @author Betsy Hilliard
 *
 */
public class ModelBasedLearningAgent extends SGAgent {

	boolean firstGame = true;
	Policy currentPlan;
	
	RandomDefaultPolicy opponentPlan;
	
	String opponentName = null;
	
	Map<State, Map<GroundedSGAgentAction,Double>> actionCounts;
	double delta = 0.99;
	double alpha = .8;
	
	@Override
	public void gameStarting() {
		// do something as a base
		if(this.firstGame){
			actionCounts = new HashMap<State, Map<GroundedSGAgentAction,Double>>();
			
			// set a random opponent policy
			// plan against that policy
			currentPlan = setPlanAgainstRandom();
			this.firstGame = false;
		}

	}

	private Policy setPlanAgainstRandom() {
		currentPlan = updatePlan();
		return currentPlan;
	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		GroundedSGAgentAction a = (GroundedSGAgentAction)currentPlan.getAction(s);
		return a;
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction,
			Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		if(opponentName == null){
			opponentName = getOpponentName(jointAction);
		}
		
		GroundedSGAgentAction otherAction = jointAction.action(opponentName);
		// add to counts of other agent's actions
		if(actionCounts.containsKey(s)){
			if(actionCounts.get(s).containsKey(otherAction)){
				actionCounts.get(s).put(otherAction, actionCounts.get(s).get(otherAction)+1);	
			} else {
				actionCounts.get(s).put(otherAction,1.0);
			}
			for(GroundedSGAgentAction oa : actionCounts.get(s).keySet()){
				actionCounts.get(s).put(oa,actionCounts.get(s).get(oa)*delta);
			}
		} else {
			Map<GroundedSGAgentAction,Double> newMap = new HashMap<GroundedSGAgentAction, Double>();
			newMap.put(otherAction, 1.0);
			actionCounts.put(s, newMap);
		}
		
		for(State os : actionCounts.keySet()){
			if(opponentLocationMatches(s,os)){
				actionCounts.get(os).put(otherAction, actionCounts.get(os).get(otherAction)+alpha);
				for(GroundedSGAgentAction oa : actionCounts.get(os).keySet()){
					actionCounts.get(s).put(oa,actionCounts.get(os).get(oa)*delta);
				}
			}
		}
	}


	private boolean opponentLocationMatches(State s, State os) {
		int sx=-2;
		int sy=-2;
		int osx=-1;
		int osy=-1;
		List<ObjectInstance> agents = s.getObjectsOfClass(GridGame.CLASSAGENT);
		for (ObjectInstance a : agents){
			if(a.getName().compareTo(opponentName)==0){
					sx = a.getIntValForAttribute(GridGame.ATTX);
					sy = a.getIntValForAttribute(GridGame.ATTY);
			}
		}
		List<ObjectInstance> agentsO = os.getObjectsOfClass(GridGame.CLASSAGENT);
		for (ObjectInstance ao : agentsO){
			if(ao.getName().compareTo(opponentName)==0){
				
					osx = ao.getIntValForAttribute(GridGame.ATTX);
					osy = ao.getIntValForAttribute(GridGame.ATTY);
			}
		}
		
		return (osx==sx && osy==sy);
	}

	private String getOpponentName(JointAction jointAction) {
		List<String> agentNames = jointAction.getAgentNames();
		for (String name : agentNames){
			if(name.compareTo(this.getAgentName()) == 0){
				return name;
			}
		}
		return null;
	}

	@Override
	public void gameTerminated() {
		// update opponent's policy
		this.opponentPlan.updatePolicyWithCounts(actionCounts);
		// generate a new plan
		currentPlan = updatePlan();
	}

	private Policy updatePlan() {
		// TODO Auto-generated method stub
		// this is the hard part!
		
		
		return null;
	}
}
