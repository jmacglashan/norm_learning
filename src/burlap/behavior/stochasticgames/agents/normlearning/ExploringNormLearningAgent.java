package burlap.behavior.stochasticgames.agents.normlearning;

import java.util.List;
import java.util.Random;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

public class ExploringNormLearningAgent extends NormLearningAgent {

	boolean firstStep = true;
	boolean useTeam = true;
	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public ExploringNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int jointPlannerH,
			int c, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, jointPlannerH, c, learnFromBadGames);
		this.useTeam = useTeam;
	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 */
	public ExploringNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, 
			ValueFunctionInitialization leafValues, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, c, leafValues, learnFromBadGames);
		this.useTeam = useTeam;
	}
	
	
	@Override
	public GroundedSGAgentAction getAction(State s) {
		
		boolean explore = false;
		Random rand = new Random();
		 
		if (this.currentGame != null && this.cgames.size()>0 && !firstStep) {
			List<JointAction> actionsTaken = this.currentGame.getJointActions();
			List<State> statesVisited = this.currentGame.getStates();
			JointAction lastJointAction = actionsTaken.get(actionsTaken.size()-1);
			State lastStateVisited = statesVisited.get(statesVisited.size()-2);
			double probJointAction = 1.0;
			double normedProbJointAction = 1.0;
			double probSingleAction = this.dp.getProbOfAction(lastStateVisited, 
					lastJointAction.action(this.getAgentName()));
					
			List<ActionProb> jointActionProbs = this.dp.getJointActionDistributionForState(lastStateVisited);
			
			for (ActionProb ap : jointActionProbs){
				JointAction ja = ((CentralizedDomainGenerator.GroundedJointActionWrapper)ap.ga).jointAction;
				//System.out.println(ja.actionName()+" Prob: "+ap.pSelection);
				if(ja.equals(lastJointAction)){
					System.out.println(ja.actionName()+" Prob: "+ap.pSelection);
					System.out.println("Found!");
					probJointAction = ap.pSelection;
					if(probSingleAction>0){
						normedProbJointAction = probJointAction/probSingleAction;
					} else {
						normedProbJointAction = 0;
					}
				}
			}
			System.out.println("Last JointAction: "+lastJointAction.actionName());
			System.out.println("Prob Last Joint Action: "+probJointAction+" Prob Agent's Last Action: "+probSingleAction);
			System.out.println("NormedProb: "+normedProbJointAction);
			double draw = rand.nextDouble();
			
			explore = draw >= normedProbJointAction;
			System.out.println("Draw = "+draw+" -> Explore: "+explore);
		}
		System.out.println("Team Policy");
		List<ActionProb> teamActionProbs = this.teamPolicy.getActionDistributionForState(s);
		for (ActionProb prob1 : teamActionProbs) {
			System.out.println("\t" + prob1.pSelection+" "+prob1.ga.actionName());
		}
		System.out.println("Learned Norm Policy");
		List<ActionProb> learnedActionProbs = this.dp.getActionDistributionForState(s);
		for (ActionProb prob : learnedActionProbs) {
			System.out.println("\t" + prob.pSelection+" "+prob.ga.actionName());
		}
		firstStep = false;
		GroundedSGAgentAction gaExp = null;
		if(explore){
			// Explore
			// This picks an action uniformly at random
			
			if(useTeam){
				gaExp = (GroundedSGAgentAction)this.teamPolicy.getAction(s);
				return gaExp;
			} else {
				gaExp = (GroundedSGAgentAction) teamActionProbs.get(rand.nextInt(teamActionProbs.size())).ga;
			}
			System.out.println("Exploring with: "+gaExp.actionName());
		}else{
			//EXPLOIT
			
			gaExp = (GroundedSGAgentAction)this.dp.getAction(s);
			System.out.println("Exploiting with: "+gaExp.actionName());
		}
		return gaExp;
		
	}
	
	@Override
	public void gameTerminated() {
		super.gameTerminated();
		firstStep = true;
	}

}
