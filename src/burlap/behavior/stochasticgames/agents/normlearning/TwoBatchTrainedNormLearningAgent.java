package burlap.behavior.stochasticgames.agents.normlearning;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;

import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.datastructures.AlphanumericSorting;
import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGDomain;

public class TwoBatchTrainedNormLearningAgent extends NormLearningAgent {

	String batchTracesFolder1;
	String batchTracesFolder2;
	int numSamples1 = -1;
	int numSamples2 = -1;
	String trial;

	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public TwoBatchTrainedNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int jointPlannerH,
			int c, String batchTracesFolder, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, jointPlannerH, c, learnFromBadGames);
		this.batchTracesFolder1 = batchTracesFolder;

	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 * @param numSamples 
	 */
	public TwoBatchTrainedNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, 
			ValueFunctionInitialization leafValues, String batchTracesFolder1, String batchTracesFolder2, 
			String trial, int numSamples1,int numSamples2, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, c, leafValues, learnFromBadGames);
		this.batchTracesFolder1 = batchTracesFolder1;
		this.batchTracesFolder2 = batchTracesFolder2;
		this.numSamples1 = numSamples1;
		this.numSamples2 = numSamples2;
		this.trial = trial;


	}


	@Override
	public void gameStarting() {
		//System.out.println("game starting");
		if(!this.started) {

			this.generateEquivelentSADomain();
			this.cgames = createEpisodeAnalysisList(1);
			cgames.addAll(createEpisodeAnalysisList(2));
			if(cgames.size()==0){
				this.dp = this.generateNoExperiencePolicy();
			}else{
				this.dp = this.generateExperiencedPolicy(cgames);
			}
			this.started = true;
		}

		this.gameStart = true;
	}


	@Override
	public void gameTerminated() {
		//System.out.println("Game terminated");
		//System.out.println(this.currentGame);
		EpisodeAnalysis ea = CentralizedDomainGenerator.gameAnalysisToEpisodeAnalysis(this.cmdpDomain, this.currentGame);
		double reward = ea.getDiscountedReturn(1.0);
		if (reward > 0.0 || learnFromBadGames) {
			//this.cgames.add(ea);
		} 
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction, Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		//System.out.println("Observing outcome");
		if(this.gameStart){
			this.currentGame = new GameAnalysis(s);
			//System.out.println(this.currentGame);
			this.gameStart = false;
		}

		this.currentGame.recordTransitionTo(jointAction, sprime, jointReward);
	}


	protected List<EpisodeAnalysis> createEpisodeAnalysisList(int setNumber){

		String batchTracesFolder;
		String matchNum;
		int numSamples;
		if(setNumber==1){
			batchTracesFolder=batchTracesFolder1;
			numSamples=numSamples1;
			matchNum="match_0";
		}else{
			batchTracesFolder=batchTracesFolder2;
			numSamples=numSamples2;
			matchNum="match_1";
		}
		if(!batchTracesFolder.endsWith("/")){
			batchTracesFolder = batchTracesFolder + "/";
		}

		File dir = new File(batchTracesFolder);
		final String ext = ".game";
		final String match = matchNum;

		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File dir, String name) {
				if(name.endsWith(ext)&& name.contains(match)&&name.contains("trial_"+trial)){
					return true;
				}
				return false;
			}
		};
		String[] children = dir.list(filter);
		//System.out.println("dir: "+dir.getPath());
		List<EpisodeAnalysis> eas = new ArrayList<EpisodeAnalysis>(children.length);

		for(int i = 0; i < children.length; i++){
			String gameFile = batchTracesFolder + children[i];
			GameAnalysis ga = GameAnalysis.parseFileIntoGA(gameFile, sgDomain);
			EpisodeAnalysis ea = CentralizedDomainGenerator.
					gameAnalysisToEpisodeAnalysis(cmdpDomain, ga);
			eas.add(ea);
		}
		//System.out.println("Read in "+eas.size()+" games.");
		Random rand = new Random();
		if(numSamples>=0){
			while(eas.size()>numSamples){
				eas.remove(rand.nextInt(eas.size()));
			}
		}
		//System.out.println("Sampled to "+eas.size()+" games.");
		return eas;
	}

}
