package burlap.behavior.stochasticgames.agents.normlearning.baselines;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.DecentralizedPolicy;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BaselineLearningAgent  extends SGAgent {

	protected SGDomain sgDomain;
	protected SADomain cmdpDomain;

	List<EpisodeAnalysis> cgames = new ArrayList<EpisodeAnalysis>();
	GameAnalysis currentGame;

	DecentralizedPolicy teamPolicy;
	ValueFunctionInitialization normLeafValues;

	boolean started = false;
	boolean gameStart = false;

	protected int jointPlannerH;
	protected int RHIRL_h = 1;
	protected int c;

	



	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public BaselineLearningAgent(SGDomain sgDomain,
			int jointPlannerH, int c) {
		this.sgDomain = sgDomain;
		this.jointPlannerH = jointPlannerH;
		this.c = c;
	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 */
	public BaselineLearningAgent(SGDomain sgDomain, int c, 
			ValueFunctionInitialization leafValues) {
		this.sgDomain = sgDomain;

		this.c = c;
		this.normLeafValues = leafValues;

	}

	public BaselineLearningAgent copy() {
		return new BaselineLearningAgent(this.sgDomain,this.c, 
				this.normLeafValues);
	}


	/**
	 * Use this to set the horizon used in RHIRL.
	 * @param RHIRL_h
	 */
	public void setRHIRL_h(int RHIRL_h) {
		this.RHIRL_h = RHIRL_h;
	}


	@Override
	public void gameStarting() {

		if(!this.started) {
			this.generateEquivelentSADomain();
			this.teamPolicy = this.generateNoExperiencePolicy();
			this.started = true;
		}

		this.gameStart = true;
	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		List<ActionProb> actionProbs = this.teamPolicy.getActionDistributionForState(s);
		for (ActionProb prob : actionProbs) {
			System.out.println("\t" + prob.toString());
		}
		return (GroundedSGAgentAction)this.teamPolicy.getAction(s);
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction, Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		if(this.gameStart){
			this.currentGame = new GameAnalysis(s);
			this.gameStart = false;
		}

		this.currentGame.recordTransitionTo(jointAction, sprime, jointReward);
	}

	@Override
	public void gameTerminated() {
		EpisodeAnalysis ea = CentralizedDomainGenerator.gameAnalysisToEpisodeAnalysis(this.cmdpDomain, this.currentGame);
		this.cgames.add(ea);
	}

	protected void generateEquivelentSADomain() {
		CentralizedDomainGenerator cmdpgen = new CentralizedDomainGenerator(this.sgDomain, new ArrayList<SGAgentType>(this.world.getAgentDefinitions().values()));
		this.cmdpDomain = (SADomain)cmdpgen.generateDomain();
	}

	protected DecentralizedPolicy generateNoExperiencePolicy() {
		JointReward jr = this.world.getRewardModel();
		TerminalFunction tf = this.world.getTF();

		TotalWelfare crf = new TotalWelfare(jr);

		if(this.normLeafValues == null) {

			final SparseSampling cmdpTWPlanner = new SparseSampling(this.cmdpDomain, crf, tf, 0.99, new SimpleHashableStateFactory(), this.jointPlannerH, this.c);
			cmdpTWPlanner.toggleDebugPrinting(false);

			this.normLeafValues = new ValueFunctionInitialization() {
				@Override
				public double qValue(State s, AbstractGroundedAction a) {
					return cmdpTWPlanner.getQ(s, a).q;
				}

				@Override
				public double value(State s) {
					return cmdpTWPlanner.value(s);
				}
			};
		}

		SparseSampling learnedPlanner = new SparseSampling(this.cmdpDomain, crf, tf, 0.99, new SimpleHashableStateFactory(), RHIRL_h, this.c);
		learnedPlanner.setValueForLeafNodes(this.normLeafValues);

		return new DecentralizedPolicy(new GreedyQPolicy(learnedPlanner), this.getAgentName());
	}

	public List<EpisodeAnalysis> getcGames(){
		return this.cgames;
	}
}

