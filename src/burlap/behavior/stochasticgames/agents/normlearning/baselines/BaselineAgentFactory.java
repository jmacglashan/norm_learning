package burlap.behavior.stochasticgames.agents.normlearning.baselines;

import java.io.FileInputStream;
import java.util.List;
import java.util.Properties;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.JointReward;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGAgentType;
import burlap.oomdp.stochasticgames.SGDomain;
import examples.GridGameNormRF2;


public class BaselineAgentFactory {

	//TODO: clean this up, probably don't need all of it...
	public static SGAgent getBaselineAgent(String parametersFile, SGDomain sgDomain, 
			List<SGAgentType> types, JointReward jr, TerminalFunction tf){

		Properties props = new Properties();
		FileInputStream fis;
		try {
			//loading properties from file
			fis = new FileInputStream(parametersFile+".properties");
			props.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}

		// convert grid game to a centralized MDP (which will be used to define
		// social reward functions)
		CentralizedDomainGenerator mdpdg = new CentralizedDomainGenerator(sgDomain, types);
		Domain cmdp = mdpdg.generateDomain();
		RewardFunction crf = new TotalWelfare(jr);

		// read from parameters file
		double gamma = Double.parseDouble(props.getProperty("gamma","0.99"));
		int h = Integer.parseInt(props.getProperty("h","20"));
		int c = Integer.parseInt(props.getProperty("c","-1"));

		// create joint task planner for social reward function and RHIRL leaf
		// node values
		final SparseSampling jplanner = new SparseSampling(cmdp, crf, tf, gamma,
				new SimpleHashableStateFactory(), h, c);
		jplanner.toggleDebugPrinting(false);

		// create agent
		BaselineLearningAgent agent;
		agent = new TeamPolicyBaseline(sgDomain, h, c);
		return agent;
	}

}
