package burlap.behavior.stochasticgames.agents.normlearning.baselines;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.DecentralizedPolicy;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TeamPolicyBaseline  extends BaselineLearningAgent {

	



	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public TeamPolicyBaseline(SGDomain sgDomain,
			int jointPlannerH, int c) {
		super(sgDomain,jointPlannerH,c);
	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 */
	public TeamPolicyBaseline(SGDomain sgDomain, int c, 
			ValueFunctionInitialization leafValues) {
		super(sgDomain,c, leafValues);

	}

	
}

