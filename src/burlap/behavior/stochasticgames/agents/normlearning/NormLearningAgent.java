package burlap.behavior.stochasticgames.agents.normlearning;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.policy.Policy.ActionProb;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.MLIRL;
import burlap.behavior.singleagent.learnfromdemo.mlirl.MLIRLRequest;
import burlap.behavior.singleagent.learnfromdemo.mlirl.differentiableplanners.DifferentiableSparseSampling;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedPolicyToJointPolicy;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.DecentralizedPolicy;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class NormLearningAgent extends SGAgent {

	protected SGDomain sgDomain;
	protected SADomain cmdpDomain;

	List<EpisodeAnalysis> cgames = new ArrayList<EpisodeAnalysis>();
	GameAnalysis currentGame;

	public DecentralizedPolicy dp;
	public Policy learnedJoint;
	DecentralizedPolicy teamPolicy;
	ValueFunctionInitialization normLeafValues;

	boolean started = false;
	boolean gameStart = false;
	boolean learnFromBadGames = false;

	protected int jointPlannerH;
	protected int RHIRL_h = 1;
	protected int c;

	protected DifferentiableRF normRF;


	protected double irlBoltzmannBeta = 1.;
	protected double irlLearningRate = 0.1;
	protected double irlLogLikelihoodChangeThreshold = 0.01;
	protected int irlMaxGradientAscentSteps = 10;




	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public NormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, 
			int jointPlannerH, int c, boolean learnFromBadGames) {
		this.sgDomain = sgDomain;
		this.normRF = normRF;
		this.jointPlannerH = jointPlannerH;
		this.c = c;
		
		this.learnFromBadGames = learnFromBadGames;
	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 */
	public NormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, 
			ValueFunctionInitialization leafValues, boolean learnFromBadGames) {
		this.sgDomain = sgDomain;
		this.normRF = normRF;
		this.c = c;
		this.normLeafValues = leafValues;
		
		this.learnFromBadGames = learnFromBadGames;
	}
	
	public NormLearningAgent copy() {
		return new NormLearningAgent(this.sgDomain, this.normRF, this.c, 
				this.normLeafValues, this.learnFromBadGames);
	}

	/**
	 * Use this to set the horizon used in RHIRL.
	 * @param RHIRL_h
	 */
	public void setRHIRL_h(int RHIRL_h) {
		this.RHIRL_h = RHIRL_h;
	}
	
	public SADomain getCmdpDomain(){
		return this.cmdpDomain;
	}


	/**
	 * Sets the IRL Parameters this agent will use
	 * @param irlBoltzmannBeta the Boltzmann distribution beta parameter; large is more deterministic; smaller is more uniform
	 * @param irlLearningRate the gradient ascent learning rate/step size
	 * @param irlLogLikelihoodChangeThreshold the threshold in log likelihood change to terminate gradient ascent
	 * @param irlMaxGradientAscentSteps the maximum number of gradient ascent steps to take when running IRL
	 */
	public void setIRLParameters(double irlBoltzmannBeta, double irlLearningRate, double irlLogLikelihoodChangeThreshold, int irlMaxGradientAscentSteps){
		this.irlBoltzmannBeta = irlBoltzmannBeta;
		this.irlLearningRate = irlLearningRate;
		this.irlLogLikelihoodChangeThreshold = irlLogLikelihoodChangeThreshold;
		this.irlMaxGradientAscentSteps = irlMaxGradientAscentSteps;
	}

	@Override
	public void gameStarting() {

		if(!this.started) {
			this.generateEquivelentSADomain();
			this.teamPolicy = this.generateNoExperiencePolicy();
			this.dp = this.teamPolicy;
			this.started = true;
		}

		this.gameStart = true;
	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		List<ActionProb> actionProbs = this.dp.getActionDistributionForState(s);
		for (ActionProb prob : actionProbs) {
			System.out.println("\t" + prob.toString());
		}
		return (GroundedSGAgentAction)this.dp.getAction(s);
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction, Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		if(this.gameStart){
			this.currentGame = new GameAnalysis(s);
			System.out.println(this.currentGame);
			this.gameStart = false;
		}

		this.currentGame.recordTransitionTo(jointAction, sprime, jointReward);
	}

	@Override
	public void gameTerminated() {
		EpisodeAnalysis ea = CentralizedDomainGenerator.gameAnalysisToEpisodeAnalysis(this.cmdpDomain, this.currentGame);
		double reward = ea.getDiscountedReturn(1.0);
		if (reward > 0.0 || learnFromBadGames) {
			this.cgames.add(ea);
		} 
		if (this.cgames.size() > 0) {
			this.dp = this.generateExperiencedPolicy(this.cgames);
		}
	}
	
	protected void generateEquivelentSADomain() {
		CentralizedDomainGenerator cmdpgen = new CentralizedDomainGenerator(this.sgDomain, new ArrayList<SGAgentType>(this.world.getAgentDefinitions().values()));
		this.cmdpDomain = (SADomain)cmdpgen.generateDomain();
	}
	
	protected DecentralizedPolicy generateNoExperiencePolicy() {
		JointReward jr = this.world.getRewardModel();
		TerminalFunction tf = this.world.getTF();
		
		TotalWelfare crf = new TotalWelfare(jr);

		if(this.normLeafValues == null) {

			final SparseSampling cmdpTWPlanner = new SparseSampling(this.cmdpDomain, crf, tf, 0.99, new SimpleHashableStateFactory(), this.jointPlannerH, this.c);
			cmdpTWPlanner.toggleDebugPrinting(false);

			this.normLeafValues = new ValueFunctionInitialization() {
				@Override
				public double qValue(State s, AbstractGroundedAction a) {
					return cmdpTWPlanner.getQ(s, a).q;
				}

				@Override
				public double value(State s) {
					return cmdpTWPlanner.value(s);
				}
			};
		}

		SparseSampling learnedPlanner = new SparseSampling(this.cmdpDomain, crf, tf, 0.99, new SimpleHashableStateFactory(), RHIRL_h, this.c);
		learnedPlanner.setValueForLeafNodes(this.normLeafValues);
		this.learnedJoint = new GreedyQPolicy(learnedPlanner);
		return new DecentralizedPolicy(new GreedyQPolicy(learnedPlanner), this.getAgentName());
	}
	
	protected DecentralizedPolicy generateExperiencedPolicy(List<EpisodeAnalysis> cgames) {
		System.out.println(this.getAgentName() + ": begin learning");
		DifferentiableSparseSampling dss = new DifferentiableSparseSampling(this.cmdpDomain, this.normRF, this.world.getTF(), 0.99, new SimpleHashableStateFactory(), this.RHIRL_h, this.c, this.irlBoltzmannBeta);
		dss.toggleDebugPrinting(false);
		dss.setValueForLeafNodes(this.normLeafValues);

		//now run IRL
		MLIRLRequest request = new MLIRLRequest(this.cmdpDomain, dss, cgames, this.normRF);
		request.setBoltzmannBeta(this.irlBoltzmannBeta);
		MLIRL irl = new MLIRL(request, this.irlLearningRate, this.irlLogLikelihoodChangeThreshold, this.irlMaxGradientAscentSteps);
		irl.toggleDebugPrinting(false);
		irl.performIRL();
		
		//setup our new policy
		SparseSampling learnedPlanner = new SparseSampling(this.cmdpDomain, this.normRF, this.world.getTF(), 0.99, new SimpleHashableStateFactory(), this.RHIRL_h, this.c);
		learnedPlanner.setValueForLeafNodes(this.normLeafValues);
		System.out.println(this.getAgentName() + ": end learning");
		this.learnedJoint = new GreedyQPolicy(learnedPlanner);
		return new DecentralizedPolicy(learnedJoint, this.getAgentName());
	}
	
	public Policy getJointPolicy(){
		return new CentralizedPolicyToJointPolicy(this.learnedJoint);
	}
	
	public List<EpisodeAnalysis> getcGames(){
		return this.cgames;
	}
}
