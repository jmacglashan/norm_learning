package burlap.behavior.stochasticgames.agents.normlearning;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.MLIRL;
import burlap.behavior.singleagent.learnfromdemo.mlirl.MLIRLRequest;
import burlap.behavior.singleagent.learnfromdemo.mlirl.differentiableplanners.DifferentiableSparseSampling;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.DecentralizedPolicy;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.oomdp.core.AbstractGroundedAction;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;
import burlap.oomdp.stochasticgames.*;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class ForeverNormLearningAgent extends NormLearningAgent {

	private List<EpisodeAnalysis> cgames;
	private List<Integer> indexesForThisAgentsGames;
	protected double weightForThisMatchEpisodes = 1.0;
		/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public ForeverNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int jointPlannerH, int c) {
		super(sgDomain, normRF, jointPlannerH, c, false);
		this.cgames = Collections.synchronizedList(new ArrayList<EpisodeAnalysis>());
		this.indexesForThisAgentsGames = Collections.synchronizedList(new ArrayList<Integer>());
		
	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 */
	public ForeverNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, ValueFunctionInitialization leafValues) {
		super(sgDomain, normRF, c, leafValues, false);
		this.cgames = Collections.synchronizedList(new ArrayList<EpisodeAnalysis>());
		this.indexesForThisAgentsGames = Collections.synchronizedList(new ArrayList<Integer>());
		
	}
	
	public ForeverNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, ValueFunctionInitialization leafValues, List<EpisodeAnalysis> previousGames) {
		super(sgDomain, normRF, c, leafValues, false);
		this.cgames = Collections.synchronizedList(new ArrayList<EpisodeAnalysis>(previousGames));
		this.indexesForThisAgentsGames = Collections.synchronizedList(new ArrayList<Integer>());
		
	}
	
	@Override
	public ForeverNormLearningAgent copy() {
		NormLearningAgent base = super.copy();
		return new ForeverNormLearningAgent(base.sgDomain, base.normRF, base.c, base.normLeafValues, this.cgames);
	}
	
	public void addGamesFromAgent(ForeverNormLearningAgent agent) {
		synchronized(this.cgames) {
			this.cgames.addAll(agent.cgames);
		}
	}

	public void setWeightForThisMatchEpisodes(double value) {
		if (value >= 0.0) {
			this.weightForThisMatchEpisodes = value;
		}
	}
	
	@Override
	public void gameStarting() {
		if(!this.started) {
			this.generateEquivelentSADomain();
			if (this.cgames.isEmpty()) {
				this.generateNoExperiencePolicy();
			} else {
				this.generateExperiencedPolicy(this.cgames);
			}
			this.started = true;
		}

		this.gameStart = true;
	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		return (GroundedSGAgentAction)this.dp.getAction(s);
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction, Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		if(this.gameStart){
			this.currentGame = new GameAnalysis(s);
			this.gameStart = false;
		}

		this.currentGame.recordTransitionTo(jointAction, sprime, jointReward);
	}

	@Override
	public void gameTerminated() {
		EpisodeAnalysis ea = CentralizedDomainGenerator.gameAnalysisToEpisodeAnalysis(this.cmdpDomain, this.currentGame);
		this.cgames.add(ea);
		
		this.generateExperiencedPolicy(this.cgames);
	}
	
	private double[] getWeights() {
		double[] weights = new double[this.cgames.size()];
		Arrays.fill(weights, 1.0);
		for (Integer i : this.indexesForThisAgentsGames) {
			weights[i] = this.weightForThisMatchEpisodes;
		}
		return weights;
	}
}
