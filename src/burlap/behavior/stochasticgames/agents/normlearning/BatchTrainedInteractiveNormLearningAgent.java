package burlap.behavior.stochasticgames.agents.normlearning;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.datastructures.AlphanumericSorting;
import burlap.oomdp.stochasticgames.SGDomain;

public class BatchTrainedInteractiveNormLearningAgent extends BatchTrainedNormLearningAgent {
	
	

	/**
	 * This constructor will automatically plan in the joint task to seed the RHIRL leaf node values by using
	 * a {@link burlap.behavior.singleagent.planning.stochastic.sparsesampling.SparseSampling} planner.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param jointPlannerH the horizon for planning the joint task
	 * @param c the transition sampling size for the joint task planner and RHIRL. Use -1 for full Bellman
	 */
	public BatchTrainedInteractiveNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int jointPlannerH,
			int c, String batchTracesFolder, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, jointPlannerH, c, batchTracesFolder, learnFromBadGames, useTeam);

	}


	/**
	 * This constructor will use the leaf node values in the RHIRL that are provided rather than creating a planner for it.
	 * RHIRL will by default use a horizon of 1 (use setter to change).
	 * @param sgDomain a {@link burlap.oomdp.stochasticgames.SGDomain}
	 * @param normRF the parameterized joint task family in which to learn
	 * @param c the transition sampling size for RHIRL. Use -1 for full Bellman
	 * @param leafValues the (potentially differentiable) leaf node values used in RHIRL
	 */
	public BatchTrainedInteractiveNormLearningAgent(SGDomain sgDomain, DifferentiableRF normRF, int c, 
			ValueFunctionInitialization leafValues, int numSamples, String batchTracesFolder,String trial, boolean learnFromBadGames, boolean useTeam) {
		super(sgDomain, normRF, c, leafValues, batchTracesFolder,trial, numSamples, learnFromBadGames, useTeam);

	}
	

	@Override
	public void gameTerminated() {
		EpisodeAnalysis ea = CentralizedDomainGenerator.gameAnalysisToEpisodeAnalysis(this.cmdpDomain, this.currentGame);
		double reward = ea.getDiscountedReturn(1.0);
		if (reward > 0.0 || learnFromBadGames) {
			this.cgames.add(ea);
		} 
		if (this.cgames.size() > 0) {
			this.dp = this.generateExperiencedPolicy(this.cgames);
		}
	}

}
