package burlap.behavior.stochasticgames.agents.normlearning.utilityagents;

import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import burlap.behavior.singleagent.learnfromdemo.mlirl.support.DifferentiableRF;
import burlap.behavior.stochasticgames.GameAnalysis;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.CentralizedDomainGenerator;
import burlap.behavior.stochasticgames.auxiliary.jointmdp.TotalWelfare;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.SGDomain;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;

public class CopyGameFilesAgent extends SGAgent {

	public CopyGameFilesAgent(String parametersFile, String outputLocation, SGDomain sgDomain, int trial) {
		// copy the files here
		Properties props = new Properties();
		FileInputStream fis;
		try {
			//loading properties from file
			fis = new FileInputStream(parametersFile+".properties");
			props.load(fis);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String gameFileLocation = props.getProperty("game_file_location", "none");
		if(gameFileLocation.compareTo("none")!=0){

			File dir = new File(gameFileLocation);
			final String ext = ".game";
			final String match = "match_0";

			FilenameFilter filter = new FilenameFilter() {
				public boolean accept(File dir, String name) {
					if(name.endsWith(ext)&& name.contains(match)){
						return true;
					}
					return false;
				}
			};
			//System.out.println("gameFileLocation: "+gameFileLocation);
			String[] children = dir.list(filter);
			List<Integer> trialNums = new ArrayList<Integer>();
			for(int i =0;i<children.length;i++){
				String[] name = children[i].split("_");
				for(int s=0;s<name.length-1;s++){
					if(name[s].compareTo("trial")==0){
						Integer trialNum = Integer.parseInt(name[s+1]);
						if(!trialNums.contains(trialNum)){
							trialNums.add(trialNum);
						}
					}
				}

			}
			Collections.sort(trialNums);
			int trialToUse = trialNums.get(trial);

			for(int i = 0; i < children.length; i++){
				if(children[i].contains("trial_"+trialToUse)){
					String gameFile = gameFileLocation + "/"+children[i];
					GameAnalysis ga = GameAnalysis.parseFileIntoGA(gameFile, sgDomain);
					ga.writeToFile(outputLocation+ "/"+children[i].replace("trial_"+trialToUse, "trial_"+trial));
				}
			}
		}


	}

	@Override
	public void gameStarting() {
		// TODO Auto-generated method stub

	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction,
			Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		// TODO Auto-generated method stub

	}

	@Override
	public void gameTerminated() {
		// TODO Auto-generated method stub

	}

}
