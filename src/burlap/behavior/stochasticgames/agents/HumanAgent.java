package burlap.behavior.stochasticgames.agents;

import burlap.oomdp.core.states.State;
import burlap.oomdp.stochasticgames.JointAction;
import burlap.oomdp.stochasticgames.SGAgent;
import burlap.oomdp.stochasticgames.agentactions.GroundedSGAgentAction;
import burlap.oomdp.visualizer.Visualizer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

/**
 * @author James MacGlashan.
 */
public class HumanAgent extends SGAgent implements KeyListener{

	Visualizer v;
	Map<Integer, GroundedSGAgentAction> keyActionMap = new HashMap<Integer, GroundedSGAgentAction>();
	JointAction lastJointAction = new JointAction();
	Map<String, Double> lastRewards = new HashMap<String, Double>();
	int confirmTerminationKey = 10; //enter

	volatile GroundedSGAgentAction chosenAction = null;
	volatile boolean confirmed = false;

	JTextArea consoleOut;

	public HumanAgent(Visualizer v) {
		this.v = v;
		this.v.addKeyListener(this);
		this.v.setPreferredSize(new Dimension(800, 800));
		JFrame frame = new JFrame();
		frame.setLayout(new BorderLayout());
		frame.add(this.v, BorderLayout.CENTER);

		this.consoleOut = new JTextArea(6, 25);
		frame.add(this.consoleOut, BorderLayout.SOUTH);

		frame.pack();
		frame.setVisible(true);

	}

	public void addKeyAction(String key, GroundedSGAgentAction action){
		int keyCode = KeyEvent.getExtendedKeyCodeForChar(key.charAt(0));
		this.keyActionMap.put(keyCode, action);
	}

	@Override
	public void gameStarting() {
		this.lastJointAction = new JointAction();
		this.lastRewards = new HashMap<String, Double>();
		chosenAction = null;
		confirmed = false;
	}

	@Override
	public GroundedSGAgentAction getAction(State s) {
		this.chosenAction = null;
		this.updateState(s);
		synchronized(this){
			while(this.chosenAction == null){
				try {
					this.wait();
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return this.chosenAction;
	}

	@Override
	public void observeOutcome(State s, JointAction jointAction, Map<String, Double> jointReward, State sprime, boolean isTerminal) {
		this.lastJointAction = jointAction;
		this.lastRewards = jointReward;
		this.updateState(sprime);
	}

	@Override
	public void gameTerminated() {
		this.confirmed = false;
		synchronized(this){
			while(!this.confirmed){
				try {
					this.wait();
				} catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	@Override
	public void keyPressed(KeyEvent e) {


		if(e.getKeyCode() == this.confirmTerminationKey){
			synchronized(this){
				confirmed = true;
				this.notifyAll();
			}
		}
		else{

			GroundedSGAgentAction selection = this.keyActionMap.get(e.getKeyCode());
			if(selection != null) {
				synchronized(this){
					this.chosenAction = selection.copy();
					this.chosenAction.actingAgent = this.getAgentName();
					this.notifyAll();
				}
			}

		}
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	protected void updateState(State s){
		this.v.updateState(s);
		String text = this.lastJointAction.toString() + "\n" + this.lastRewards.toString();
		this.consoleOut.setText(text);
	}
}