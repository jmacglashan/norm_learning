package burlap.behavior.singleagent.auxiliary.pruneddomain;

import burlap.oomdp.auxiliary.DomainGenerator;
import burlap.oomdp.core.Attribute;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.ObjectClass;
import burlap.oomdp.core.TransitionProbability;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.FullActionModel;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.SADomain;
import burlap.oomdp.singleagent.environment.Environment;
import burlap.oomdp.singleagent.environment.EnvironmentOutcome;

import java.util.List;

/**
 * @author James MacGlashan.
 */
public class PrunedActionDomain implements DomainGenerator {

	protected Domain sourceDomain;
	protected GroundedActionGenerator actionPruner;

	public PrunedActionDomain(Domain sourceDomain, GroundedActionGenerator actionPruner) {
		this.sourceDomain = sourceDomain;
		this.actionPruner = actionPruner;
	}

	@Override
	public Domain generateDomain() {

		SADomain nDomain = new SADomain();
		for(ObjectClass oc : this.sourceDomain.getObjectClasses()){
			nDomain.addObjectClass(oc);
		}
		for(Attribute a : this.sourceDomain.getAttributes()){
			nDomain.addAttribute(a);
		}

		for(Action a : this.sourceDomain.getActions()){
			PrunedAction.generatePrunedAction(nDomain, a, this.actionPruner);
		}

		return nDomain;
	}



	public static class PrunedAction extends Action{

		protected Action delegate;
		protected GroundedActionGenerator generator;


		public static PrunedAction generatePrunedAction(Domain domain, Action delegate, GroundedActionGenerator generator){
			if(delegate instanceof FullActionModel){
				return new PrunedActionWithFullModel(domain, delegate, generator);
			}
			return new PrunedAction(domain, delegate, generator);
		}

		protected PrunedAction(Domain domain, Action delegate, GroundedActionGenerator generator){
			super(delegate.getName(), domain);
			this.delegate = delegate;
			this.generator = generator;
		}

		@Override
		public boolean applicableInState(State s, GroundedAction groundedAction) {
			return false;
		}

		@Override
		public boolean isPrimitive() {
			return this.delegate.isPrimitive();
		}

		@Override
		public boolean isParameterized() {
			return this.delegate.isParameterized();
		}

		@Override
		public GroundedAction getAssociatedGroundedAction() {
			return this.delegate.getAssociatedGroundedAction();
		}

		@Override
		public List<GroundedAction> getAllApplicableGroundedActions(State s) {
			List<GroundedAction> gas = this.generator.getAllApplicableGroundedActions(this.delegate, s);
			return gas;
		}

		@Override
		public State performAction(State s, GroundedAction groundedAction) {
			return this.delegate.performAction(s, groundedAction);
		}

		@Override
		public EnvironmentOutcome performInEnvironment(Environment env, GroundedAction groundedAction) {
			return this.delegate.performInEnvironment(env, groundedAction);
		}

		@Override
		protected State performActionHelper(State s, GroundedAction groundedAction) {
			throw new RuntimeException("performActionHelper should not be called directly for PrunedAction class");
		}


		public static class PrunedActionWithFullModel extends PrunedAction implements FullActionModel{

			public PrunedActionWithFullModel(Domain domain, Action delegate, GroundedActionGenerator generator) {
				super(domain, delegate, generator);
			}

			@Override
			public List<TransitionProbability> getTransitions(State s, GroundedAction groundedAction) {
				return ((FullActionModel)this.delegate).getTransitions(s, groundedAction);
			}
		}
	}


}
