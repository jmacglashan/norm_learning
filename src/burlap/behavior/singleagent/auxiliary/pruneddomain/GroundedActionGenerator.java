package burlap.behavior.singleagent.auxiliary.pruneddomain;

import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;

import java.util.List;

/**
 * @author James MacGlashan.
 */
public interface GroundedActionGenerator {
	List<GroundedAction> getAllApplicableGroundedActions(Action action, State s);
}
