package burlap.behavior.singleagent.auxiliary.pruneddomain;

import burlap.behavior.policy.GreedyQPolicy;
import burlap.behavior.policy.Policy;
import burlap.behavior.singleagent.EpisodeAnalysis;
import burlap.behavior.singleagent.planning.stochastic.rtdp.BoundedRTDP;
import burlap.behavior.singleagent.planning.stochastic.valueiteration.ValueIteration;
import burlap.behavior.valuefunction.ValueFunctionInitialization;
import burlap.domain.singleagent.gridworld.GridWorldDomain;
import burlap.domain.singleagent.gridworld.GridWorldTerminalFunction;
import burlap.oomdp.core.Domain;
import burlap.oomdp.core.TerminalFunction;
import burlap.oomdp.core.states.State;
import burlap.oomdp.singleagent.Action;
import burlap.oomdp.singleagent.GroundedAction;
import burlap.oomdp.singleagent.RewardFunction;
import burlap.oomdp.singleagent.common.UniformCostRF;
import burlap.oomdp.statehashing.SimpleHashableStateFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author James MacGlashan.
 */
public class PolicyPrunner implements GroundedActionGenerator {

	protected Policy p;
	protected State lastQueryState = null;
	protected Set<GroundedAction> nonZeroActions = new HashSet<GroundedAction>();


	public PolicyPrunner(Policy p) {
		this.p = p;
	}

	@Override
	public List<GroundedAction> getAllApplicableGroundedActions(Action action, State s) {

		if(s != lastQueryState){
			this.nonZeroActions.clear();
			List<Policy.ActionProb> aps = p.getActionDistributionForState(s);
			for(Policy.ActionProb ap : aps){
				if(ap.pSelection > 0.){
					nonZeroActions.add((GroundedAction)ap.ga);
				}
			}
		}

		List<GroundedAction> gas = new ArrayList<GroundedAction>(nonZeroActions.size());
		for(GroundedAction nza : this.nonZeroActions){
			if(nza.action.equals(action)){
				gas.add(nza);
			}
		}

		return gas;
	}


	public static void main(String[] args) {

		GridWorldDomain gwd = new GridWorldDomain(11, 11);
		gwd.setMapToFourRooms();
		Domain domain = gwd.generateDomain();
		TerminalFunction tf = new GridWorldTerminalFunction(10, 10);
		RewardFunction rf = new UniformCostRF();
		State s = GridWorldDomain.getOneAgentNoLocationState(domain, 0, 0);


		ValueIteration vi = new ValueIteration(domain, rf, tf, 0.99, new SimpleHashableStateFactory(), 0.001, 100);
		GreedyQPolicy p = vi.planFromState(s);

		PolicyPrunner pp = new PolicyPrunner(p);
		PrunedActionDomain pad = new PrunedActionDomain(domain, pp);
		Domain dprime = pad.generateDomain();


		/*
		List<GroundedAction> gas = Action.getAllApplicableGroundedActionsFromActionList(dprime.getActions(), s);
		for(GroundedAction ga : gas){
			System.out.println(ga.toString());
		}
		*/

		BoundedRTDP boundedRTDP = new BoundedRTDP(dprime, rf, tf, 0.99, new SimpleHashableStateFactory(),
				new ValueFunctionInitialization.ConstantValueFunctionInitialization(-100),
				new ValueFunctionInitialization.ConstantValueFunctionInitialization(),
				0.01,
				1);
		boundedRTDP.setMaxRolloutDepth(25);


		Policy bp = boundedRTDP.planFromState(s);
		EpisodeAnalysis ea = bp.evaluateBehavior(s, rf, tf, 100);
		System.out.println(ea.getActionSequenceString("\n"));


	}
}
